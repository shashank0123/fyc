
<?php 
use App\Models\Category;
use App\Models\Product;
use App\Wishlist;
use App\Models\Company;
use App\Models\Headline;

$contact = [];
$company = Company::first();
if(!empty($company))
  $contact = explode(',',$company->contact_no);
$count = 0;

if(!empty(session()->get('cart'))){
  foreach(session()->get('cart') as $cart){
    $count++;
  }
}

$headline = Headline::orderBy('created_at','DESC')->first();

$categories = Category::where('status','Active')->where('category_id','0')->orderBy('created_at','DESC')->get();
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta name="keywords" content="HomeGlare-Ecommerce"> 

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>FYC Professional | @yield('page_title')</title>
  <meta name="description" content="@yield('page_discription')">
  <link rel="canonical" href="@yield('page_URL')" />
  <!-- meta tags for social share -->
  <meta property="og:image" content="@yield('social_image')">
  <meta property="og:title" content="@yield('social_title')">
  <meta property="og:description" content="@yield('social_description')">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="/fyc-new/images/logo/logo.png">
    <!-- CSS
      ============================================ -->
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="/fyc-new/css/vendor/bootstrap.min.css">
      <!-- Icon Font CSS -->
      <link rel="stylesheet" href="/fyc-new/css/vendor/line-awesome.css">
      <link rel="stylesheet" href="/fyc-new/css/vendor/themify.css">
      <!-- othres CSS -->
      <link rel="stylesheet" href="/fyc-new/css/plugins/animate.css">
      <link rel="stylesheet" href="/fyc-new/css/plugins/owl-carousel.css">
      <link rel="stylesheet" href="/fyc-new/css/plugins/slick.css">
      <link rel="stylesheet" href="/fyc-new/css/plugins/magnific-popup.css">
      <link rel="stylesheet" href="/fyc-new/css/plugins/jquery-ui.css">
      <link rel="stylesheet" href="/fyc-new/css/style.css">
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
      <style>
      .swal2-popup .swal2-styled.swal2-confirm { background-color: #000033 !important }
    </style>
  </head>
  <body onload="checkCart()">
    <div class="main-wrapper">
      <header class="header-area bg-black sticky-bar header-padding-1">

        <div class="main-header-wrap">
          <div class="top-head" style="background-image: url('/fyc/top-head.png');">
            <div class=" container-fluid row">
              <div class="col-sm-9">
                <marquee style="margin-top: 7px; margin-left: 10%"><b>{{$headline->headline ?? ''}}</b></marquee>
              </div>
              <div class="col-sm-3">
                <ul>
                  <li><a href="https://www.facebook.com/yavicosmetics19"><i class='fab fa-facebook-f'></i></a></li>
                  <li><a href="https://twitter.com/NehaYavi"><i class='fab fa-twitter'></i></a></li>
                  <li><a href="https://www.instagram.com/yavicosmetics/?hl=en"><i class='fab fa-instagram'></i></a></li>
                  <li><a href="https://www.youtube.com/channel/UCtNU6IP7S11_CzuJQDf95UQ"><i class='fas fa-rss'></i></a></li>
                  <li><a href="https://linkedin.com/https://in.pinterest.com/yavicosmetics02"><i class='fab fa-pinterest-p'></i></a></li>
                  <li><a href=""><i class='fab fa-linkedin-in'></i></a></li>

                </ul>
              </div>
            </div>
          </div>

          <div class="container-fluid">
            <!-- Top-Header -->
            <div class="row top-header">
              <div class="col-sm-3 main-logo">
                <a href="/">
                  <img  height="60px" alt="" src="/fyc-new/images/logo/logo.png">
                </a>
              </div>
              <div class="col-sm-6 main-search">
                <input type="search" class="form-control search-bar" name="search" placeholder="Search Product">
              </div>
              <div class="col-sm-3 cart-div">
                <ul>
                  <li>
                    <a href="tel::@if($contact){{$contact[0]}}@endif"><i class="fas fa-phone-alt"></i>@if($contact)+91 {{$contact[0]}}@endif</a>
                  </li>

                  <li onclick="showMiniCart()"><i class="fas fa-heart"></i><sup id="wishItem"><?php if(!empty(Auth::user()->id))
                  {$wish_count = Wishlist::where('user_id',Auth::user()->id)->count();
                  echo $wish_count;
                }
                else { echo 0; }
                ?></sup></li>
                <li onclick="showMiniCart()">
                  <div class="cart-wrap common-style">
                    <span class="cart-active" onclick="showMiniCart()">
                      <i class="fas fa-shopping-bag"></i><sup id="cartItem">0</sup>
                    </span>
                    <div class="shopping-cart-content" id="miniCart">
                    </div>
                  </div>
                </li>

              </ul>
            </div>
          </div>

          <!-- Bottom Header -->
          <div class="row align-items-center bottom-header">
            <div class="col-xl-2 col-lg-2 logo-border">

            </div>
            <div class="col-xl-8 col-lg-8 d-flex justify-content-center">
              <div class="main-menu menu-common-style menu-lh-2 menu-margin-2 menu-white">
                <nav>
                  <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/about">About us </a></li>
                    <li class="angle-shape"><a href="#">Products <i class="fa fa-angle-down"></i></a>
                      <ul class="submenu">
                        @if(!empty($categories))
                        @foreach($categories as $category)
                        <li><a href="{{url('/products/'.$category->slug)}}">{{ucfirst($category->category_name)}}</a>

                          <?php
                          $cate = Category::where('category_id',$category->id)->get();
                          ?>                         

                          @if(count($cate)>0)

                          <ul class="submenu1">

                            @foreach($cate as $cat)
                            <li> <a href="{{url('/products/'.$cat->slug)}}">{{$cat->category_name}}</a>
                              <?php
                              $sub_cate = Category::where('category_id',$cat->id)->get();
                              ?>
                              @if(count($sub_cate)>0)
                              <ul class="submenu1">
                                @foreach($sub_cate as $sub)
                                <li><a href="{{url('/products/'.$sub->slug)}}">- {{$sub->category_name}}</a></li>
                                @endforeach
                              </ul>
                              @else
                              <?php
                              $products = Product::where('category_id',$cat->id)->orderBy('created_at','DESC')->limit(5)->get();
                              ?>
                              @if(count($products)>0)
                              @php $countp = 0 @endphp
                              <ul class="submenu1">
                                @foreach($products as $product)
                                @php $countp++ @endphp
                                <li><a href="/product-detail/{{$product->slug}}">{{ucfirst($product->name)}}</a></li>
                                @endforeach
                                @if($countp >=5)
                                <li style="text:align: center"><a href="{{url('products/'.$cat->slug)}}" style="color: #993366">View More...</a></li>
                                @endif
                              </ul>
                              @endif  
                              @endif
                            </li>
                            @endforeach

                          </ul>

                          @else
                          <?php
                          $products = Product::where('category_id',$category->id)->orderBy('created_at','DESC')->limit(5)->get();
                          ?>
                          @if(count($products)>0)
                          @php $countp = 0 @endphp
                          <ul class="submenu1">
                            @foreach($products as $product)
                            @php $countp++ @endphp
                            <li><a href="/product-detail/{{$product->slug}}">{{ucfirst($product->name)}}</a></li>
                            @endforeach
                            @if($countp >=5)
                            <li style="text:align: center"><a href="{{url('products/'.$category->slug)}}" style="color: #993366">View More...</a></li>
                            @endif
                          </ul>
                          @endif
                          @endif
                        </li>
                        @endforeach
                        @endif
                      </ul>


                    </li>
                    <!-- <li><a href="/blogsw">Blog</a></li> -->
                    <li><a href="/offers">Offers</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                    <li><a href="@if(!empty(\Auth::user()->id)){{'/checkout/'.\Auth::user()->id}}@else{{'/login'}}@endif">Buy Now</a></li>
                    <!-- <li><a href="/sell-on-homeglare">Sell on Homeglare</a></li> -->
                    @if(!empty(Auth::user()->id))
                    <li class="angle-shape">
                      <a href="#">My Account</a>
                      <ul class="submenu">
                        <li><a href="/account/{{Auth::user()->id}}">Dashboard</a></li>
                        <li class="active">
                          <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          {{'Logout'}}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                        </form>
                      </li>
                    </ul>
                  </li>
                  @else
                  <li><a href="/login">Login</a></li>

                  @endif
                  {{-- 
                    <li class="angle-shape">
                      <a href="shop.html">Shop </a>
                      <ul class="mega-menu">
                        <li>
                          <a class="menu-title" href="#">Shop Layout</a>
                          <ul>
                            <li><a href="shop.html">standard style</a></li>
                            <li><a href="shop-2.html">standard style 2</a></li>
                            <li><a href="shop-2-col.html">shop 2 column</a></li>
                            <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                            <li><a href="shop-fullwide.html">shop fullwide</a></li>
                          </ul>
                        </li>
                        <li>
                          <a class="menu-title" href="#">Shop Layout</a>
                          <ul>
                            <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                            <li><a href="shop-list.html">list style</a></li>
                            <li><a href="shop-list-2col.html">list 2 column</a></li>
                            <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>
                          </ul>
                        </li>
                        <li>
                          <a class="menu-title" href="#">Product Details</a>
                          <ul>
                            <li><a href="product-details.html">standard style</a></li>
                            <li><a href="product-details-2.html">standard style 2</a></li>
                            <li><a href="product-details-tab1.html">tab style 1</a></li>
                            <li><a href="product-details-tab2.html">tab style 2</a></li>
                            <li><a href="product-details-tab3.html">tab style 3 </a></li>
                          </ul>
                        </li>
                        <li>
                          <a class="menu-title" href="#">Product Details</a>
                          <ul>
                            <li><a href="product-details-gallery.html">gallery style </a></li>
                            <li><a href="product-details-sticky.html">sticky style</a></li>
                            <li><a href="product-details-slider.html">slider style</a></li>
                            <li><a href="product-details-affiliate.html">Affiliate style</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    --}}
                  </ul>
                </nav>
              </div>
            </div>
            <div class="col-xl-2 col-lg-2 header-right-border">

            </div>
          </div>
        </div>
        <!-- main-search start -->
        <div class="main-search-active">
          <div class="sidebar-search-icon">
            <button class="search-close"><span class="la la-close"></span></button>
          </div>
          <div class="sidebar-search-input">
            <form action="/global-search-product" method="POST">
              <div class="form-search">
                <input id="search" class="input-text" value="" placeholder="Search Now" type="search" name="product_keyword">
                <button>
                  <i class="la la-search"></i>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="top-head2" style="background-color: #ddd">
            <!-- <a href="whatsapp://send?abid=7289800098&text=Hey%2C%20User!">
              <img src="{{asset('/logo/icon.png')}}" style="width: 25px; height: auto">
            7289800098</a> -->
          </div>
          <div class="header-small-mobile header-mobile-white">

            <div class="container-fluid">
              <div class="row align-items-center">
                <div class="col-6">
                  <div class="mobile-logo">
                    <a href="/">
                      <img  height="40px" alt="" src="/fyc-new/images/logo/logo.png">
                    </a>
                  </div>
                </div>
                <div class="col-6">
                  <div class="header-right-wrap mr-10">
                    <div class="cart-wrap common-style">
                      <button class="cart-active" style="margin-top:15px;font-size:18px;">
                        <i class="la la-heart-o"></i>
                        <!--  <span class="count-style">2 Items</span> -->
                        <sup>
                          <p class="cartsup" id="wishItem2" style="text-align:center;">0</p>
                        </sup>
                      </button>
                    </div>
                    <div class="cart-wrap common-style">
                      <button class="cart-active" style="margin-top:15px;font-size:19px;" onclick="showMiniCart()">
                        <i class="la la-shopping-cart"></i>
                        <!--  <span class="count-style">2 Items</span> -->
                        <sup>
                          <p class="cartsup" id="cartItem2" style="text-align:center;">0</p>
                        </sup>
                      </button>
                      <div class="shopping-cart-content">
                        <div class="shopping-cart-top">
                          <h4>Your Cart</h4>
                          <a class="cart-close" href="#"><i class="la la-close"></i></a>
                        </div>
                        <ul>
                          <li class="single-shopping-cart">
                            <div class="shopping-cart-img">
                              <a href="#"><img alt="" src="/fyc-new/images/cart/cart-1.jpg"></a>
                              <div class="item-close">
                                <a href="#"><i class="sli sli-close"></i></a>
                              </div>
                            </div>
                            <div class="shopping-cart-title">
                              <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                              <span>₹99.00</span>
                            </div>
                            <div class="shopping-cart-delete">
                              <a href="#"><i class="la la-trash"></i></a>
                            </div>
                          </li>
                          <li class="single-shopping-cart">
                            <div class="shopping-cart-img">
                              <a href="#"><img alt="" src="/fyc-new/images/cart/cart-2.jpg"></a>
                              <div class="item-close">
                                <a href="#"><i class="sli sli-close"></i></a>
                              </div>
                            </div>
                            <div class="shopping-cart-title">
                              <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                              <span>₹99.00</span>
                            </div>
                            <div class="shopping-cart-delete">
                              <a href="#"><i class="la la-trash"></i></a>
                            </div>
                          </li>
                          <li class="single-shopping-cart">
                            <div class="shopping-cart-img">
                              <a href="#"><img alt="" src="/fyc-new/images/cart/cart-3.jpg"></a>
                              <div class="item-close">
                                <a href="#"><i class="sli sli-close"></i></a>
                              </div>
                            </div>
                            <div class="shopping-cart-title">
                              <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                              <span>₹99.00</span>
                            </div>
                            <div class="shopping-cart-delete">
                              <a href="#"><i class="la la-trash"></i></a>
                            </div>
                          </li>
                        </ul>
                        <div class="shopping-cart-bottom">
                          <div class="shopping-cart-total">
                            <h4>Subtotal <span class="shop-total">₹290.00</span></h4>
                          </div>
                          <div class="shopping-cart-btn btn-hover default-btn text-center">
                            <a class="black-color" href="/checkoutw">Continue to Chackout</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="mobile-off-canvas">
                      <a class="mobile-aside-button" href="#"><i class="la la-navicon la-2x"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div class="mobile-off-canvas-active">
          <a class="mobile-aside-close"><i class="la la-close"></i></a>
          <div class="header-mobile-aside-wrap">
            <div class="mobile-search">
              <form class="search-form" action="#">
                <input type="text" placeholder="Search entire store…">
                <button class="button-search"><i class="la la-search"></i></button>
              </form>
            </div>
            <div class="mobile-menu-wrap">
              <!-- mobile menu start -->
              <div class="mobile-navigation">
                <!-- mobile menu navigation start -->
                <nav>
                  <ul class="mobile-menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="/about">About us </a></li>

                    <li class="menu-item-has-children "><a href="#">Products</a>
                      <ul class="dropdown">

                        @if(!empty($categories))
                        @foreach($categories as $category)
                        <li><a href="{{url('/products/'.$category->slug)}}">{{ucfirst($category->category_name)}}</a></li>
                        @endforeach
                        @endif                      

                        <!-- <li class="menu-item-has-children"><a href="#">shop layout</a>
                          <ul class="dropdown">
                            <li><a href="shop.html">standard grid style</a></li>
                            <li><a href="shop-2.html">standard style 2</a></li>
                            <li><a href="shop-2-col.html">shop 2 column</a></li>
                            <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                            <li><a href="shop-fullwide.html">shop fullwide</a></li>
                            <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                          </ul>
                        </li> -->

                      </ul>
                    </li>                    

                    <li><a href="/offers">Offers</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                    <li><a href="@if(!empty(\Auth::user()->id)){{'/checkout/'.\Auth::user()->id}}@else{{'/login'}}@endif">Buy Now</a></li>                    

                  </ul>
                </nav>
                <!-- mobile menu navigation end -->
              </div>
              <!-- mobile menu end -->
            </div>
            <div class="mobile-curr-lang-wrap">
              <div class="single-mobile-curr-lang">
                @if(!empty(Auth::user()->id))

                <a class="mobile-account-active" href="#">My Account <i class="la la-angle-down"></i></a>
                <div class="lang-curr-dropdown account-dropdown-active">
                  <ul>
                    <li><a href="/account/{{Auth::user()->id}}">Dashboard</a></li>
                    <li class="active">
                      <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{'Logout'}}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </li>
                </ul>
              </div>
              @else
              <a href="{{url('login')}}">Login</a>
              @endif
            </div>
          </div>
          <div class="mobile-social-wrap">
            <a href="https://www.facebook.com/homeglareonline" class="facebook" data-toggle="tooltip" target="_blank" title="Facebook"><i class=" ti-facebook"></i></a>
            <a class="twitter" href="https://www.twitter.com" data-toggle="tooltip" target="_blank" title="Twitter"><i class="ti-twitter-alt"></i></a>
            {{-- <a class="pinterest" href="#"><i class="ti-pinterest"></i></a> --}}
            {{-- <a class="google" href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus"><i class="ti-google"></i></a> --}}
            <a class="instagram" href="https://www.instagram.com" data-toggle="tooltip" target="_blank" title="Instagram"><i class="ti-instagram"></i></a>
          </div>
        </div>
      </div>
      <br>
      @yield('content')

      <footer class="footer-area">
        <div class="feature-area ">
          <div class="container-fluid">
            <div class="feature-border feature-border-about">
              <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="row">
                    <div class="col-sm-3 order-div">
                      <img src="/fyc-new/images/icon-img/feature-icon-1.png" alt="">
                    </div>
                    <div class="col-sm-9">
                      <div class="feature-wrap mb-30">
                        <h5><b>Free Shipping</b></h5>
                        <span>Free shipping on all US order or order above $200</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="row">
                    <div class="col-sm-3 order-div">
                      <img src="/fyc-new/images/icon-img/feature-icon-2.png" alt="">
                    </div>
                    <div class="col-sm-9">
                      <div class="feature-wrap mb-30">
                        <h5><b>Support 24/7</b></h5>
                        <span>Contact us 24 hours a day, 7 days a week</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="row">
                    <div class="col-sm-3 order-div">
                      <img src="/fyc-new/images/icon-img/feature-icon-3.png" alt="">
                    </div>
                    <div class="col-sm-9">
                      <div class="feature-wrap mb-30">
                        <h5><b>30 Days Return</b></h5>
                        <span>Simply return it within 30 days for an exchange</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="row">
                    <div class="col-sm-3 order-div">
                      <img src="/fyc-new/images/icon-img/feature-icon-4.png" alt="">
                    </div>
                    <div class="col-sm-9">
                      <div class="feature-wrap mb-30">
                        <h5><b>100% Payment Secure</b></h5>
                        <span>We ensure secure payment with PEV</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> 
        <div class="footer-top pt-105 footer-overlay">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget footer-contact-wrap-2 mb-40">
                  <h3>About Us</h3>
                  <!-- <a href="#"><img src="/fyc-new/images/logo/logo.png" alt="logo"></a> -->
                  <div class="footer-contact-content-2">
                    <p>Yavi Cosmetics focuses on modernity the development of its products is based on "High-Tech" advanced active cosmetology, based on natural ingredients and special techniques, which offer effective andvisible results. All the products are bio-compatible (i.e. formulated with substances equivalent to the skin). </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-30">
                  <div class="footer-title-2">
                    <h3>Important Links</h3>
                  </div>
                  <div class="footer-list-2">
                    <ul>
                      <li><a href="{{url('/about')}}">About Us</a></li>
                      <li><a href="{{url('/terms&conditions')}}">Terms & Conditions</a></li>
                      <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                      <li><a href="{{url('/return-policy')}}">Return Policy</a></li>
                      <!-- <li><a href="{{url('/faq')}}">FAQ</a></li> -->
                      @if(!empty(Auth::user()->id))
                      <li><a href="{{url('/account/'.Auth::user()->id)}}">My Account</a></li>
                      @endif
                      @if(!empty(Auth::user()->id))
                      <li><a href="{{url('/cart/'.Auth::user()->id)}}">Shopping Cart</a></li>
                      @else
                      <li><a href="{{route('login')}}">Shopping Cart</a></li>
                      @endif
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-30">
                  <div class="footer-title-2">
                    <h3>Find Us On Map</h3>
                  </div>
                  <div class="footer-list-2">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3499.384234952276!2d77.11124831458092!3d28.708060982388083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d01577860cd6f%3A0xf64de02056f4860d!2s29%2C%209%2C%20G%20Block%20Rd%2C%20Naharpur%20Village%2C%20Rohini%2C%20Delhi%2C%20110085!5e0!3m2!1sen!2sin!4v1576585715640!5m2!1sen!2sin" width="100%" height="200px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-10">
                  <div class="footer-title-2">
                    <h3>Contact Us</h3>
                  </div>
                  <div class="footer-contact-wrap">

                    <p>
                      <div class="row">
                        <div class="col-sm-2"><i class="fas fa-home"></i></div>
                        <div class="col-sm-10"> @if($company && $company->address)
                          <?php echo $company->address; ?><br>
                        @endif</div>
                      </div>
                    </p>

                    <p>
                      <div class="row">
                        <div class="col-sm-2"><i class="fas fa-phone-alt"></i></div>
                        <div class="col-sm-10">
                          @if(!empty($contact))
                          @foreach($contact as $con)
                          <a href="tel://{{$con}}">+91-{{$con}}</a><br>&nbsp;&nbsp;&nbsp;
                          @endforeach
                          @endif
                        </div>
                      </div>                      
                    </p>

                    <p> 
                      <div class="row">
                        <div class="col-sm-2"><i class="fas fa-envelope"></i></div>
                        <div class="col-sm-10"><a href="mailto://info@fycprofessional.com">info@fycprofessional.com</a></div>
                      </div>
                    </p>
                    
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="footer-bottom ptb-30">
            <div class="container">
              <div class="copyright copyright-2 text-center">
                <p>Copyright © <a href="https://www.fycprofessional.com/">Fycprofessional</a>. Design and Developed by <a href="https://www.backstagesupporters.com/">Backstagesupporters</a>.</p>
              </div>
            </div>
          </div>
        </footer>
        
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
          var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
          (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5cd9527c2846b90c57ae3a64/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
          })();
        </script>
        <!--End of Tawk.to Script-->
        

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="tab-content quickview-big-img">
                      <div id="pro-1" class="tab-pane fade show active">
                        <img src="" alt="" id="pro1">
                      </div>
                      <div id="pro-2" class="tab-pane fade">
                        <img src="" alt="" id="pro2">
                      </div>
                      <div id="pro-3" class="tab-pane fade">
                        <img src="" alt="" id="pro3">
                      </div>
                      <div id="pro-4" class="tab-pane fade">
                        <img src="" alt="" id="pro4">
                      </div>
                    </div>
                    <!-- Thumbnail Large Image End -->
                    <!-- Thumbnail Image End -->
                    <div class="quickview-wrap mt-15">
                      <div class="quickview-slide-active owl-carousel nav nav-style-2" role="tablist">
                        <a class="active" data-toggle="tab" href="#pro-1"><img src="" alt="" id="pro5"></a>
                        <a data-toggle="tab" href="#pro-2"><img src="" alt="" id="pro6"></a>
                        <a data-toggle="tab" href="#pro-3"><img src="" alt="" id="pro7"></a>
                        <a data-toggle="tab" href="#pro-4"><img src="" alt="" id="pro8"></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <div class="product-details-content quickview-content">
                      <span id="pdCategory">Life Style</span>
                      <h2 id="pdName">LaaVista Depro, FX 829 v1</h2>
                      {{-- 
                        <div class="product-ratting-review">
                          <div class="product-ratting">
                            <i class="la la-star"></i>
                            <i class="la la-star"></i>
                            <i class="la la-star"></i>
                            <i class="la la-star"></i>
                            <i class="la la-star-half-o"></i>
                          </div>
                          <div class="product-review">
                            <span>40+ Reviews</span>
                          </div>
                        </div>
                        --}}
                    <!--  <div class="pro-details-color-wrap">
                      <span>Color:</span>
                      <div class="pro-details-color-content">
                          <ul>
                              <li class="green"></li>
                              <li class="yellow"></li>
                              <li class="red"></li>
                              <li class="blue"></li>
                          </ul>
                      </div>
                      </div>
                      <div class="pro-details-size">
                      <span>Size:</span>
                      <div class="pro-details-size-content">
                          <ul>
                              <li><a href="#">s</a></li>
                              <li><a href="#">m</a></li>
                              <li><a href="#">xl</a></li>
                              <li><a href="#">xxl</a></li>
                          </ul>
                      </div>
                    </div> -->
                    <div class="pro-details-price-wrap">
                      <div class="product-price">
                        <span id="pdPrice">₹210.00</span>
                        <span class="old" id="pdMrp">₹230.00</span>
                      </div>
                      <div class="dec-rang" ><span id="pdDiscount">- 30%</span></div>
                    </div>
                    {{-- 
                      <div class="pro-details-quality">
                        <div class="cart-plus-minus">
                          <input class="cart-plus-minus-box" type="text" name="qtybutton" value="1">
                        </div>
                      </div>
                      --}}
                      <div class="pro-details-compare-wishlist">
                        <div class="pro-details-buy-now btn-hover btn-hover-radious">
                          <a id="pdAddToCart">Add To Cart</a>
                        </div>
                        <br>
                        <div class="ml-3 mb-3 pro-details-buy-now btn-hover btn-hover-radious">
                          <a title="Add To Wishlist" id="pdaddToWishlist"><i class="la la-heart-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal end -->



      </div>
    <!-- JS
      ============================================ -->
      <!-- Modernizer JS -->
      <script src="/fyc-new/js/vendor/modernizr-3.6.0.min.js"></script>
      <!-- Modernizer JS -->
      <script src="/fyc-new/js/vendor/jquery-3.3.1.min.js"></script>
      <!-- Popper JS -->
      <script src="/fyc-new/js/vendor/popper.js"></script>
      <!-- Bootstrap JS -->
      <script src="/fyc-new/js/vendor/bootstrap.min.js"></script>
      <!-- Slick Slider JS -->
      <script src="/fyc-new/js/plugins/countdown.js"></script>
      <script src="/fyc-new/js/plugins/counterup.js"></script>
      <script src="/fyc-new/js/plugins/images-loaded.js"></script>
      <script src="/fyc-new/js/plugins/isotope.js"></script>
      <script src="/fyc-new/js/plugins/instafeed.js"></script>
      <script src="/fyc-new/js/plugins/jquery-ui.js"></script>
      <script src="/fyc-new/js/plugins/jquery-ui-touch-punch.js"></script>
      <script src="/fyc-new/js/plugins/magnific-popup.js"></script>
      <script src="/fyc-new/js/plugins/owl-carousel.js"></script>
      <script src="/fyc-new/js/plugins/scrollup.js"></script>
      <script src="/fyc-new/js/plugins/waypoints.js"></script>
      <script src="/fyc-new/js/plugins/slick.js"></script>
      <script src="/fyc-new/js/plugins/wow.js"></script>
      <script src="/fyc-new/js/plugins/textillate.js"></script>
      <script src="/fyc-new/js/plugins/elevatezoom.js"></script>
      <script src="/fyc-new/js/plugins/sticky-sidebar.js"></script>
      <script src="/fyc-new/js/plugins/smoothscroll.js"></script>

      <!-- Main JS -->
      <script src="/fyc-new/js/main.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>

      
      @yield('script')


      <script>
        function getWishlist(){
         var data = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id ;} else {echo '0';}?>;
         if(data > 0){
          window.location.href = '/wishlist/'+data;
        }
        else{
          window.location.href = '/login';

        }
      }
      
      function showMiniCart(){
        $('.shopping-cart-content').show();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
         url: '/show-minicart',
         type: 'GET',
         data: {_token: CSRF_TOKEN},
         success: function (data) {
          $('#miniCart').html(data);
          $("#total-bill").text(data.bill);
        }
      });
      }
      
      function deleteProduct(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      // var std = $("#show-total").html();
      // count_product--;
      // alert(id);
      
      $.ajax({
        /* the route pointing to the post function */
        url: '/cart/delete-product/'+id,
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) {
          $('#listhide'+id).hide();
          $('#cart'+id).hide();
          $("#show-total").html(data.showcount);
          $("#total-bill").html(data.bill+".00");

          $('#cartItem').html(data.cartItem);
          $('#cartItem2').html(data.cartItem);            
        }
      });
    }

    function checkCart(){
      var count = <?php echo $count;?>;
      // alert(count);
      $('#cartItem').html(count);
      $('#cartItem2').html(count);
    }


    $('#exampleModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var productId = button.data('myid')
      var modal = $(this)
      // get product details data
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
      $.ajax({
        url: '/get-product_details-data/'+productId,
        type: 'GET',
        data: {_token: CSRF_TOKEN},
        success: function (response) {
          // var responseObj = JSON.parse(data);
          data = response.data;
          category = response.category;

          $("#pdName").html(data.name);
          $("#pdDiscount").text('-'+response.discount+'%');
          $("#pdCategory").text(category.category_name);
          $("#pdPrice").html('Price: ₹'+data.sell_price);
          $("#pdMrp").html('MRP: ₹'+data.mrp);
          $("#pdName").attr('href', '/product-detail/'+data.slug);

          $("#pro1").attr('src', '/'+data.image1);
          $("#pro2").attr('src', '/'+data.image2);
          $("#pro3").attr('src', '/'+data.image1);
          $("#pro4").attr('src', '/'+data.image3);

          $("#pro5").attr('src', '/'+data.image1);
          $("#pro6").attr('src', '/'+data.image2);
          $("#pro7").attr('src', '/'+data.image1);
          $("#pro8").attr('src', '/'+data.image3);
          if(data.product_brand)
            $("#pdBrand").html('Brand: '+ data.product_brand);
          // $("#pdProductCode").html('Product Code: Product '+ data.id);
          // $("#pdReward").html('Reward Points: '+ data);
          if(data.availability == 'yes')
           $("#pdAvailability").html("Availability: In Stock");
         else
           $("#pdAvailability").html("Availability: Out of Stock");

         $("#pdAddToCart").attr('onclick', 'addToCart('+data.id+')');
         $("#pdaddToWishlist").attr('onclick', 'addToWishlist('+data.id+')');
       }
     });
    })

  </script>


</body>
</html>