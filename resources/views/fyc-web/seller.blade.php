@extends('layouts.fyc')

@section('content')

 	<div class="sell-area bg-img" style="background-image:url(/fyc-new/images/bg/breadcrumb.jpg);">
 		<div class="container">
 			<div class="sell-area-text text-center">
 				<h2> "Welcome To Homeglare Store !<br> Register Here your shop for selling your Product om Homeglare."</h2>

 				<button type="button" class="btn btn-large my-4 sell-btn btn-danger">Start selling</button>

 				<p>399 a month + additional selling fees</p>
 			</div>
 		</div>
 	</div>



 	<div class="section-title-2 my-4 text-center">
 		<h2>Start selling on Homeglare today</h2>
 		<img src="/fyc-new/images/icon-img/title-shape.png" alt="icon-img">
 	</div>


 	<div class="container">
 		<div class="row">

 			<div class="col-lg-4">
 				<div class="sellsec-3">
 					<div class="sellsec-3-img">
 						<img src="/fyc-new/images/icon-img/repairing-service.jpeg">
 					</div>
 					<h2>SERVICE</h2>
 					<p>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured</p>
 					
 				</div>
 				
 			</div>

 			<div class="col-lg-4">
 				<div class="sellsec-3">
 					<div class="sellsec-3-img">
 						<img src="/fyc-new/images/icon-img/hand.jpeg">
 					</div>
 					<h2>PRICE</h2>
 					<p> We checks each value, every day to ensure we can bring our clients the most recent offers and extraordinary low costs.</p>
 					
 				</div>
 				
 			</div>


 			<div class="col-lg-4">
 				<div class="sellsec-3">
 					<div class="sellsec-3-img">
 						<img src="/fyc-new/images/icon-img/conv.jpeg">
 					</div>
 					<h2>CONVEYANCE</h2>
 					<p> We pride ourselves on our conveyance administration where our group will convey your machine into a room of your decision. We guarantee clarifies our whole conveyance process and what makes us stand apart from the group.</p>
 					
 				</div>
 				
 			</div>

 		</div>
 		
 	</div>



 	<!-- Boxed Gradient CTA -->
 	<div class="container my-4">
 		<section class="as-cta-boxed-gradient my-4 p-4">
 			<div class="row">
 				<div class="col-sm-12 col-md-12 col-lg-8">
 					<div class="as-cta-desc mb-3">
 						
 						<h2 class="font-weight-normal">Just have a few items to sell?</h2>
 						<p class="mb-0">Please Note: All new Selling on Homeglare subscriptions include access to sell on homeglare.com, Fees, available product categories, and selling requirements may vary between the  marketplaces. </p>
 					</div>
 				</div>
 				<div class="col-sm-12 col-md-12 col-lg-4 align-self-center">
 					<div class="as-cta-btn">
 						<a href="#" class="btn btn-warning btn-lg"> Sign up to individual seller › <i class="fa fa-angle-right" aria-hidden="true"></i></a>
 					</div>
 				</div>
 			</div>
 		</section>
 	</div>
 	<!-- Boxed Gradient CTA -->
 	

 	


@endsection