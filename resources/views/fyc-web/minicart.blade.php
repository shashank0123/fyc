<?php
use App\Review;
$total = 0;
?>


<div class="shopping-cart-top">
    <h4>Your Cart</h4>
    <a class="cart-close" href="#"><i class="la la-close"></i></a>
</div>
@if($cartproducts != null)
<?php $i=0;  ?>
<ul>
    <div class="container">
               @foreach($cartproducts as $product)
               <span class="single-shopping-cart" id="listhide{{$product->id}}">
        <div class="row">
            <div class="col-sm-5">
                <div class="shopping-cart-img">
                    <a href="/product/{{$product->slug}}"><img alt="" src="{{url('/')}}/{{$product->image1}}" style="width: 100px; height: auto\"></a>
                    <div class="item-close">
                        <a onclick="deleteProduct({{$product->id}})"><i class="sli sli-close"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="shopping-cart-title">
                    <h4><a href="/product/{{$product->slug}}">{{ucfirst(substr($product->name,0,18))}}.</a></h4>
                    <span>₹{{$product->sell_price}}.00 * {{$quantity[$i]}}</span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="shopping-cart-delete">
                    <a onclick="deleteProduct({{$product->id}})"><i class="la la-trash" style="
                    margin-left: 10px;"></i></a>
                </div>

            <?php
            $total  =$total + $product->sell_price * $quantity[$i];
            $i++;
            ?>
        </div>
    </div>
</span>
            @endforeach
</div>
</ul>
@endif
<div class="shopping-cart-bottom">
    <div class="shopping-cart-total">
        <h4>Subtotal <span class="shop-total" >₹<span id="total-bill">{{$total}}.00</span></span></h4>
    </div>
    <div class="shopping-cart-btn btn-hover default-btn text-center">
      <a href="<?php if(!empty(Auth::user()->id)){echo '/cart/'.Auth::user()->id;} else{ echo '/login'; } ?>" class="black-color">Cart</a>
  </div>
  <div class="shopping-cart-btn btn-hover default-btn text-center">
    <a class="black-color" href="<?php if(!empty(Auth::user()->id)){echo '/checkout/'.Auth::user()->id;} else{ echo '/login'; } ?>">Continue to Checkout</a>
</div>
</div>
