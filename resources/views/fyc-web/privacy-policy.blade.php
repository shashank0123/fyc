@extends('layouts/fyc')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 terms">
            <br>
            <h2>Privacy Policy</h2>
            <br>


            <h4>Introduction</h4>

<p>The domain name <a href="www.fycprofessional.com">www.fycprofessional.com</a> is owned and operated by <b>FYC PROFESSIONAL</b>. We value the trust you have in us. Therefore, we insist upon the highest standards for secure transactions and customer information privacy.</p>

 <p>Complete website design and product photography was done by ‘<b>FYC PROFESSIONAL</b>’ and they are holding all the copyrights for the same. Not even a single design element, color combinations, color percentages, fonts and logo will be copied or miss used by any other party. If anyone does it then, ‘<b>FYC PROFESSIONAL</b>’ will take legal actions against that company.</p>

<p>This privacy policy sets out how uses and protects any information that you give when you use this website. is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes.</p>


<p><b>Security </b> : We endeavor to keep your Personal and Sensitive information secure, up-to-date, accurate in the best possible manner as it may be necessary for the purpose for which it was collected. We value the importance you attach to your Personal and Sensitive Information given to us and therefore we have (i) taken all reasonable measures and precautions to keep such information safe and secure and to prevent any unauthorized access to or misuse of the same; and (ii) enable you to review and edit the same.</p>

<p>However, we shall not be liable to any user for any loss, damage (whether direct, indirect, consequential or incidental) or harm caused to the user due to the unauthorized access or misuse of the Personal or Sensitive Information by any third party.</p>


<h4>HOW WE RETAIN OF SENSITIVE PERSONAL DATA OR INFORMATION.</h4>

<p>We will retain your Personal and Sensitive Information only as long as it is reasonably required or otherwise permitted or required by applicable law or regulatory requirements. We may also retain your Personal and Sensitive Information so long as it is necessary to fulfil the purposes for which it was collected (including for purposes of meeting any legal, administrative, accounting, or other reporting requirements). Your Personal and Sensitive Information is safeguarded against inappropriate access and disclosure, as per this Privacy Policy.</p>

<p>We also maintain appropriate and adequate administrative, technical and physical safeguards designed to protect your Personal and Sensitive Information against accidental, unlawful or unauthorized destruction, loss, alteration, access, disclosure or use.</p>

<h4>What we collect</h4>
<p>We may collect the following information:</p>
<ul>
<li>Name</li>
<li>Contact information including email address</li>
<li>Demographic information such as postcode, preferences and interests</li>
<li>Other information relevant to customer surveys and/or offers</li>
<li>For the exhaustive list of cookies we collect see the List of cookies we collect section.</li>
</ul>
<h4>What we do with the information we gather</h4>
<p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>
<ul>
<li>Internal record keeping.</li>
<li>We may use the information to improve our products and services.</li>

<li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</li>

<li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customise the website according to your interests.</li>
</ul>

<h4>Security</h4>
<p>We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>

<h4>Links to other websites</h4>
<p>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.
Controlling your personal information.</p>

<p>You may choose to restrict the collection or use of your personal information in the following ways:</p>

<p>Whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes.</p>

<p>If you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at <a href="mailto:admin@fycprofessional.com">admin@fycprofessional.com.</a></p>

<p>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>

<p>You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to.</p>

<p>If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>



<br><br>
  





</div>
<div class="col-sm-1"></div>
</div>
</div>







<!-- <div class="about-us-area pt-90 pb-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @if($companyInfo)
                <h2><span>Privacy Policy</span></h2><br>
                <p class="short_desc"><?php echo $companyInfo->privacy_policy; ?> </p>
                @else
                <h2>Welcome To <span>{{env('APP_NAME')}}</span></h2>
                @endif
            </div>
        </div>
    </div>
</div>
-->
@endsection
