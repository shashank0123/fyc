@php $count=0 @endphp
@if(!empty($searched_products))
@foreach($searched_products as $product)
@php $count++ @endphp
<div class="cards col-sm-4 col-md-4">
   <div class="card-content">
      <div class="icon-div">
         <i class='far fa-heart' onclick="addToWishlist({{$product->id}})"></i>
         <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->id}}"><i class='fas fa-eye' onclick="quickView({{$product->id}})"></i></a>
      </div>
      <a href="/product-detail/{{$product->slug}}"><img src="{{asset($product->image1)}}" style="height: auto; width: 100%"></a>
      <div class="card-text">
         <p class="product-name">{{ucfirst(substr($product->name,0,20))}}</p>
         <p class="product-cost">Rs. {{$product->sell_price}}</p>

      </div>
      <div class="add-cart-btn">
         <button class="cart-btn" onclick="addToCart({{$product->id}})">Add to Cart</button>
      </div>
   </div>
</div>
@endforeach
@endif

@if($count==0)
<div style="text-align: center !important;margin-top: 50px"><h2>No product found</h2></div>
@endif

