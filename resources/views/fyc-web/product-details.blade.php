<?php
use App\Models\Product;
use App\Models\Review;
?>
@extends('layouts.fyc')
<style>
    label { color: #000033 !important; }
    .star s:hover,
    .star s.active { color: #000033; }
    .star-rtl s:hover,
    .star-rtl s.active { color: #37bc9b; }
    .star s, .star-rtl s { color: black; font-size: 50px; cursor: default; text-decoration: none; line-height: 50px; }
    .star { padding: 2px; }
    .star-rtl { background: #555; display: inline-block; border: 2px solid #444; }
    .star-rtl s { color: #000033; }
    .star s:hover:before, .star s.rated:before, .star s.active:before { content: "\2605";
    font-size: 32px; }
    .star s:before { content: "\2606"; font-size: 32px; }
    .star-rtl s:hover:after, .star-rtl s.rated:after, .star-rtl s.active:after { content: "\2605"; font-size: 32px; }
    .star-rtl s:after { content: "\2606"; }
    .active { color: #000033  !important; }
    .fa-star { color: #ccc ; }
</style>
<!-- seo and social share content -->
@if($product)
@section('title', $product->name)
@endif

@if($product)
@section('social_title', $product->name)
@section('social_description', $product->long_descriptions)
@section('social_image',  env('APP_URL').'/'.$product->image1 )
@endif

@section('content')              

<div class="product-details-area pt-90 pb-90" style="width:100%; overflow:hidden;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product-details-img">
                    <div class="zoompro-border zoompro-span">
                        <img class="zoompro" src="{{url('/'.$product->image1)}}" data-zoom-image="{{url('/'.$product->image1)}}" alt="" /> 
                    </div>
                    <div id="gallery" class="mt-20 product-dec-slider">
                        <a data-image="{{url('/'.$product->image1)}}" data-zoom-image="{{url('/'.$product->image1)}}">
                            <img src="{{url('/'.$product->image1)}}" alt="" style="height: 100px; width: 100px">
                        </a>
                        <a data-image="{{url('/'.$product->image2)}}" data-zoom-image="{{url('/'.$product->image2)}}">
                            <img src="{{url('/'.$product->image2)}}" alt="" style="height: 100px; width: 100px">
                        </a>
                        <a data-image="{{url('/'.$product->image1)}}" data-zoom-image="{{url('/'.$product->image1)}}">
                            <img src="{{url('/'.$product->image1)}}" alt="" style="height: 100px; width: 100px">
                        </a>
                        <a data-image="{{url('/'.$product->image3)}}" data-zoom-image="{{url('/'.$product->image3)}}">
                            <img src="{{url('/'.$product->image3)}}" alt="" style="height: 100px; width: 100px">
                        </a>
                                {{-- <a data-image="/fyc-new/images/product-details/product-detalis-l5.jpg" data-zoom-image="/fyc-new/images/product-details/product-detalis-bl5.jpg">
                                    <img src="/fyc-new/images/product-details/product-detalis-s5.jpg" alt="">
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="product-details-content pro-details-content-modify">
                            <span>{{strtoupper($category->category_name)}}</span>
                            <h2>{{strtoupper($product->name)}}</h2>
                            <div class="product-ratting-review">
                                <div class="product-ratting">
                                    @for($i=0;$i<5;$i++)
                                    @if($productReview-- >0)
                                    <i class="la la-star"></i>
                                    @else
                                    <i class="la la-star-o"></i>
                                    @endif
                                    @endfor
                                </div>
                                <div class="product-review">
                                    <span>{{$countReview}} @if($countReview==1){{'Review'}}@else{{'Reviews'}}@endif</span>
                                </div>
                            </div>
                           <!--  <div class="pro-details-color-wrap">
                                <span>Color:</span>
                                <div class="pro-details-color-content">
                                    <ul>
                                        <li class="green"></li>
                                        <li class="yellow"></li>
                                        <li class="red"></li>
                                        <li class="blue"></li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- <div class="pro-details-size">
                                <span>Size:</span>
                                <div class="pro-details-size-content">
                                    <ul>
                                        <li><a href="#">s</a></li>
                                        <li><a href="#">m</a></li>
                                        <li><a href="#">xl</a></li>
                                        <li><a href="#">xxl</a></li>
                                    </ul>
                                </div>
                            </div> -->
                            <div class="pro-details-price-wrap">
                                <div class="product-price">
                                    <span> ₹{{$product->sell_price}}.00</span>
                                    <span class="old"> ₹{{$product->mrp}}.00</span>
                                </div>
                                <div class="dec-rang"><span>- 

<?php 
$number = (($product->mrp - $product->sell_price)/$product->mrp)*100;
 ?>

                                   {{ number_format((float)$number, 2, '.', '')}}%
</span></div>
                            </div>
                            @if(!empty($product->product_brand))
                            <div class="pro-details-price-wrap">
                                <div class="product-price">
                                 <b>Brands</b> : {{strtoupper($product->product_brand)}}
                             </div>

                         </div>
                         @endif

                         <div class="pro-details-price-wrap">
                            <div class="product-price">
                             <b>Availability</b> : <span style="
    font-size: 14px;
    color: #62ef1a;
">
                                <?php if($product->availability == 'yes'){echo 'In Stock';} else{echo 'Out of Stock';}?>
                            </span>
                         </div>

                     </div>

                     <div class="pro-details-quality">
                        <div class="cart-plus-minus">
                            <input class="cart-plus-minus-box"  type="text" name="qtybutton " value="1"id="quantity{{$product->id}}">
                        </div>
                    </div>
                    <div class="pro-details-compare-wishlist">
                     <div class="pro-details-buy-now btn-hover btn-hover-radious">
                        <a onclick="addToCart({{$product->id}},@if(!empty(Auth::user()->id)) {{Auth::user()->id}} @endif)">Add To Cart</a>
                    </div>
                    <div class="pro-details-buy-now btn-hover btn-hover-radious">
                        &nbsp;&nbsp;&nbsp;&nbsp;<a onclick="buyNow({{$product->id}},@if(!empty(Auth::user()->id)) {{Auth::user()->id}} @endif)">Buy Now</a>
                    </div>
                    <br>
                    <div class="ml-3 mb-3 pro-details-buy-now btn-hover btn-hover-radious">
                        <a title="Add To Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                    </div>
                </div>
                <div id="messageBox" class="alert alert-success" style="display: none;">Product addedd successfully.</div>


            </div>
        </div>
    </div>
</div>
</div>


<div class="description-review-wrapper pb-90">
    <div class="container">
        <div class="row">
            <div class="ml-auto mr-auto col-lg-12">
                <div class="dec-review-topbar nav mb-40">
                    <a class="active" data-toggle="tab" href="#des-details1">Description</a>
                    <!--  <a class="active" data-toggle="tab" href="#des-details2">Specification</a> -->
                    <a data-toggle="tab" href="#des-details3">Reviews</a>
                </div>
                <div class="tab-content dec-review-bottom">
                    <div id="des-details1" class="tab-pane active">
                        <div class="description-wrap">
                            <p>{{htmlspecialchars($product->long_descriptions)}}</p>
                        </div>
                    </div>
                           <!--  <div id="des-details2" class="tab-pane active">
                                <div class="specification-wrap table-responsive">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td class="width1">Name / Model</td>
                                                <td>LaaVista Depro, FX 829 v1</td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>Categories</td>
                                            </tr>
                                            <tr>
                                                <td class="width1">Models</td>
                                                <td>FX 829 v1</td>
                                            </tr>
                                            <tr>
                                                <td class="width1">Categories</td>
                                                <td>Product Type</td>
                                            </tr>
                                            <tr>
                                                <td class="width1">Size</td>
                                                <td>60’’ x 40’’</td>
                                            </tr>
                                            <tr>
                                                <td class="width1">Display Port</td>
                                                <td>Multi</td>
                                            </tr>
                                            <tr>
                                                <td class="width1">Media</td>
                                                <td>Brightside</td>
                                            </tr>
                                            <tr>
                                                <td class="width1">Color</td>
                                                <td>Black, White</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div> -->
                            <div id="des-details3" class="tab-pane">
                                @if(!empty($reviews))
                                <?php $i=1; ?>
                                @foreach($reviews as $review)
                                <?php $rate = $review->rating;
                                $date = explode(' ',$review->created_at)[0];
                                ?>
                                <div class="dec-review-wrap mb-50">
                                    <div class="row">
                                        <div class="col-xl-3 col-lg-4 col-md-5">
                                            <div class="dec-review-img-wrap">
                                                <div class="dec-review-img" STYLE="COLOR: #000033">
                                                    {{$i++}}
                                                    {{-- <img src="/fyc-new/images/product-details/review-1.png" alt="review"> --}}
                                                    
                                                </div>
                                                <div class="dec-client-name">
                                                    <h4>{{ucfirst($review->name)}}</h4>
                                                    <h6>{{$review->email}}</h6>
                                                    <div class="dec-client-rating">
                                                        @for($j=0;$j<5;$j++)
                                                        @if($rate-- >0)
                                                        <i class="la la-star"></i>
                                                        @else
                                                        <i class="la la-star-o"></i>
                                                        @endif
                                                        @endfor                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-9 col-lg-8 col-md-7">
                                            <div class="dec-review-content">
                                                <p>
                                                    @if($review->review !=null){{$review->review}}@endif</p>
                                                    <div class="review-content-bottom">
                                                        <div class="review-like">
                                                            {{-- <span><i class="la la-heart-o"></i> 24 Likes</span> --}}
                                                        </div>
                                                        <div class="review-date">
                                                            <span>{{$date}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif

                                    <div class="row">

                                        <div class="review-sec"  style="width: 100% ; padding: 41px; border:1px solid #eee; ">
                                            <h3>Write a review</h3>
                                            <form id="form-review" action="{{route('give-product-rating')}}" method="post" enctype="multipart/form-data">
                                                <input type="text" name="product_id" hidden value="{{$product->id}}">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Your Name </label>
                                                    <input type="text" class="form-control" name="name" id="exampleFormControlInput1" placeholder="">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Email address</label>
                                                    <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="name@example.com">
                                                </div>


                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Share your opinion</label>
                                                    <textarea class="form-control" name="review" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                </div>

                                                <div class="rarting">
                                                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                                  <script>
                                                    $(function() {
                                                        $("div.star > s").on("click", function(e) {

                                                                    // remove all active classes first, needed if user clicks multiple times
                                                                    $(this).closest('div').find('.active').removeClass('active');

                                                                    $(e.target).parentsUntil("div").addClass('active'); // all elements up from the clicked one excluding self
                                                                    $(e.target).addClass('active');  // the element user has clicked on


                                                                    var numStars = $(e.target).parentsUntil("div").length+1;
                                                                    $('.show-result input').val(numStars );
                                                                });
                                                    });
                                                </script>

                                                <div class="star"><s><s><s><s><s></s></s></s></s></s></div>
                                                <div class="show-result">
                                                    <input type="hidden" name="rating" id="rating" >
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                        </form>
                                    </div>


                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="mb-50 mt-40">
           <div class="section-title-2 text-center mt-4 ">
             <h2>Related products</h2>
             <img src="/fyc-new/images/icon-img/title-shape.png" alt="icon-img">
         </div>
         <div class="box-slider-active owl-carousel">
            @if($related_products !=null)
            @foreach($related_products as $product)
            <div class="product-wrap product-border-1 product-img-zoom">
              <div class="product-img">
               <a href="/product-detail/{{$product->slug}}"><img src="{{url('/'.$product->image1)}}" alt="product"></a>
               <div class="product-action-2">
                <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
            </div>
        </div>
        <div class="product-content product-content-padding">
           <h4><a href="/product-detail/{{$product->slug}}">{{substr($product->name,0,20)}}.</a></h4>
           <div class="price-addtocart">
            <div class="product-price">
             <span>₹{{$product->sell_price}}.00</span>
         </div>
     </div>
 </div>
</div>
@endforeach
@endif

</div>
</div>

@endsection


@section ('script')
<script>


function buyNow(id){
    addToCart(id);
    userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?>
    
    if(userId==0){
    window.location.href = '/login?page=checkout';
    }
    else{
     window.location.href = '/checkout/'+userId;   
    }
}

    function addToCart(id){
        var quantity = $('#quantity'+id).val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // Swal(id+" / "+quantity );

        $.ajax({
           url: '/add-to-cart',
           type: 'POST',
           data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
           success: function (data) {
           // Swal(data.cartItem);
           $('#cartItem').html(data.cartItem)
           $('#cartItem2').html(data.cartItem)


           $("#messageBox").hide().slideDown();
           setTimeout(function(){
              $("#messageBox").hide();        
          }, 3000);

       },
       failure: function (data) {
         Swal(data.message);
     }
 });
    }

    function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
    }
    else{
        // Swal(id);

        $.ajax({
           url: '/add-to-wishlist',
           type: 'POST',
           data: {_token: CSRF_TOKEN, id: id, userId : userId},
           success: function (data) {
           // Swal(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
       },
       failure: function (data) {
         Swal(data);
     }
 });
    }
}
</script>

@endsection