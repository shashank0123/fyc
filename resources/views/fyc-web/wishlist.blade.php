<?php
use App\Models\Product;
?>
@extends('layouts.fyc')

@section('content')
<style>
    #img-response{ width: 100px; height: 100px }
</style>

 
        <div class="cart-main-area pt-85 pb-90">
            <div class="container-fluid">
                <h3 class="cart-page-title">Your Wishlist items</h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <form action="#">
                            <div class="table-content table-responsive cart-table-content">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Product Name</th>
                                            <th>Until Price</th>
                                            <th>Quantity</th>
                                            <th>Stock Status</th>
                                            <th>Add To Cart</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($wishlist))
                                @foreach($wishlist as $list)
                                <?php $item = Product::where('id',$list->product_id)->first(); ?>
                                        <tr id="div-list{{$list->id}}">
                                            <td class="product-thumbnail">
                                                <a href="/product-detail/{{$item->slug}}"><img src="{{url('/')}}/{{$item->image1}}" alt="" id="img-response"></a>
                                            </td>
                                            <td class="product-name"><a href="/product-detail/{{$item->slug}}">{{$item->name}}</a></td>
                                            <td class="product-price-cart"><span class="amount">₹260.00</span></td>
                                            <td class="product-quantity">
                                                <div class="cart-plus-minus">
                                                    <input class="cart-plus-minus-box" type="text" name="qtybutton" value="1" id="quantity{{$item->id}}">
                                                </div>
                                            </td>
                                            <td class="product-subtotal">{{$item->availability}}</td>
                                            <td class="product-wishlist-cart">
                                                <a onclick="addToCart({{$item->id}},{{$list->id}})">add to cart</a>
                                            </td>
                                            <td><a onclick="deleteWishlistItem({{$list->id}})"><b><i class="la la-close"></i></b></a></td>
                                        </tr>    
                                        @endforeach
                                        @endif                                    
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')

    <script>

        function addToCart(id,list){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          var quantity = $('#quantity'+id).val();

          // alert(id+" / "+quantity);

          $.ajax({
             url: '/add-to-cart',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
             success: function (data) {
               // alert(data.cartItem);
               $('#cartItem').html(data.cartItem);

           },
           failure: function (data) {
               // alert(data.message);
           }
       });
      }

      function deleteWishlistItem(id){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          user_id = {{Auth::user()->id}};
          // alert(id+" / "+user_id);

          $.ajax({
             url: '/delete-wishlist-item',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, user_id : user_id},
             success: function (data) {
               // alert(data.flag);
               $('#cartItem').html(data.cartItem);
               $('#wishItem').html(data.wishItem);

                $('#div-list'+id).hide();
           },
           failure: function (data) {
               // alert(data.message);
           }
       });
      }
  </script>

  @endsection