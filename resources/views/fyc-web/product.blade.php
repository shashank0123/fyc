@extends('layouts.fyc')

@section('content')

<style type="text/css">
   .bg-color { background-color: #000033!important; }

   @media screen and (min-width: 991px)  { 
      #desktop-view{ display: block!important; }
      #mobile-view{ display: none!important; }
   }

   @media screen and (max-width: 991px)  {
    #desktop-view{ display: none!important; }
    #mobile-view{ display: block!important; }
 } 

</style>

<div class="shop-area pt-90 pb-90">
   <div class="container-fluid">
      <div class="row ">

      	<div class="col-lg-3">
            
               <input type="text" name="category" hidden id="category" value="{{$id}}">
    <input type="text" name="pro-color" hidden id="pro-color" value="">
    <input type="text" name="pro-rating" hidden id="pro-rating" value="">
    <input type="text" name="min-price" hidden id="min-price" value="0">
    <input type="text" name="max-price" hidden id="max-price" value="1000">
              <div class="category-menu-wrap" id="mobile-view"  >
               <button type="button" class="btn btn-block bg-color  mb-3 btn-lg">
                  <h3 class="showcat">
                     <a href="#">
                     <img class="category-menu-non-stick" src="/fyc-new/images/icon-img/category-menu.png" alt="icon">
                     <img class="category-menu-stick" src="/fyc-new/images/icon-img/category-menu-stick.png" alt="icon">
                     FILTER <i class="la la-angle-down"></i>
                     </a>
                  </h3>
               </button>
               <div class="category-menu mobile-category-menu mobile-view-filter hidecat"style="display: none;" >
                  <nav>
                     <ul><?php $c = 0; ?>
                                    @if(!empty($cat_ids))
                                    @foreach($cat_ids as $cat)

                                    <li>
                                       <a data-toggle="collapse" data-parent="#faq" href="/product/{{$cat->slug}}">{{$cat->category_name}} </a>
                                       {{-- <a data-toggle="collapse" data-parent="#faq" href="#shop-catigory-1">{{$cat->category_name}} <i class="la la-angle-down"></i></a> --}}
                                       
                                    </li>
                                    @endforeach
                                    @endif
                                    {{-- <li>
                                       <a data-toggle="collapse" data-parent="#faq" href="#shop-catigory-2">Men Fashion <i class="la la-angle-down"></i></a>
                                       <ul id="shop-catigory-2" class="panel-collapse collapse">
                                          <li><a href="#">Shirt </a></li>
                                          <li><a href="#">Shoes</a></li>
                                          <li><a href="#">Sunglasses </a></li>
                                          <li><a href="#">Sweater </a></li>
                                          <li><a href="#">Jacket </a></li>
                                       </ul>
                                    </li> --}}
                     </ul>
                  </nav>
               </div>
            </div>


                        <div class="sidebar-wrapper" id="desktop-view">
                            {{-- <div class="sidebar-widget">
                                <h4 class="sidebar-title">Search </h4>
                                <div class="sidebar-search mb-40 mt-20">
                                    <form class="sidebar-search-form" action="#">
                                        <input type="text" placeholder="Search here...">
                                        <button>
                                            <i class="la la-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div> --}}
                            <div class="sidebar-widget shop-sidebar-border pt-40">
                                <h4 class="sidebar-title">Shop By Categories</h4>
                                <div class="shop-catigory mt-20">
                                    <ul id="faq">
                                        <?php $c = 0; ?>
                                    @if(!empty($cat_ids))
                                    @foreach($cat_ids as $cat)

                                    <li>
                                       <a href="/product/{{$cat->slug}}">{{$cat->category_name}}{{--  <i class="la la-angle-down"></i> --}}</a>
                                       {{-- <a data-toggle="collapse" data-parent="#faq" href="#shop-catigory-1">{{$cat->category_name}} <i class="la la-angle-down"></i></a> --}}
                                       
                                    </li>
                                    @endforeach
                                    @endif


                                    {{-- <li>
                                       <a data-toggle="collapse" data-parent="#faq" href="#shop-catigory-2">Men Fashion <i class="la la-angle-down"></i></a>
                                       <ul id="shop-catigory-2" class="panel-collapse collapse">
                                          <li><a href="#">Shirt </a></li>
                                          <li><a href="#">Shoes</a></li>
                                          <li><a href="#">Sunglasses </a></li>
                                          <li><a href="#">Sweater </a></li>
                                          <li><a href="#">Jacket </a></li>
                                       </ul>
                                    </li> --}}
                                    </ul>
                                </div>
                            </div>
                            <div class="shop-price-filter mt-35 shop-sidebar-border pt-40 sidebar-widget">
                                <h4 class="sidebar-title">Price Filter</h4>
                                <div class="price-filter mt-20">
                                    {{-- <span>Range:  $100.00 - 1.300.00 </span> --}}
                                    <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" ><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 66.6667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 66.6667%;"></span></div>
                                    <div class="price-slider-amount">
                                        <div class="label-input">
                                            <input type="text" id="amount" name="price" placeholder="Add Your Price" >
                                        </div>
                                        <button type="button" onclick="getRange()">Filter</button>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($brand))
                           <div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
                              <h4 class="sidebar-title">Brand </h4>
                              <div class="sidebar-widget-list mt-20" style="max-height: 300px;  overflow-y: auto">
                                 <ul>
                                  @foreach($brand as $bran)
                                  <li>
                                   <a href="javascript:void(0)">{{$bran}}</a>
                                </li>
                                @endforeach
                             </ul>
                          </div>
                       </div>
                       @endif
                            @if(!empty($material))
                     <div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
                        <h4 class="sidebar-title">Material </h4>
                        <div class="sidebar-widget-list mt-20" style="max-height: 300px; overflow-y: auto">
                           <ul>
                            @foreach($material as $mate)
                            <li>
                             <a href="javascript:void(0)">{{$mate}}</a>
                          </li>
                          @endforeach
                       </ul>
                    </div>
                 </div>
                 @endif
                            @if(!empty($color))        
                 <div class="sidebar-widget pt-40 mt-40 shop-sidebar-border">
                  <h4 class="sidebar-title">Colour </h4>
                  <div class="sidebar-widget-list mt-20" style="max-height: 300px; overflow-y: auto">
                     <ul>
                        @foreach($color as $col)
                        <li>
                          <a href="javascript:void(0)" class="colorOnClick" id="{{$col}}">{{$col}}</a>
                       </li>
                       @endforeach                       
                    </ul>
                 </div>
              </div>
              @endif
                            
                        </div>
           
   <div class="sidebar-wrapper" id="desktop-view"  >
      
      
            


               {{-- <div class="sidebar-widget pt-40 mt-40 shop-sidebar-border">
                  <h4 class="sidebar-title">Size </h4>
                  <div class="sidebar-widget-list mt-20">
                     <ul>
                        <li>
                           <div class="sidebar-widget-list-left">
                              <input type="checkbox" value=""> <a href="#">XL <span>4</span> </a>
                              <span class="checkmark"></span>
                           </div>
                        </li>
                        <li>
                           <div class="sidebar-widget-list-left">
                              <input type="checkbox" value=""> <a href="#">L <span>5</span> </a>
                              <span class="checkmark"></span>
                           </div>
                        </li>
                        <li>
                           <div class="sidebar-widget-list-left">
                              <input type="checkbox" value=""> <a href="#">SM <span>6</span> </a>
                              <span class="checkmark"></span>
                           </div>
                        </li>
                        <li>
                           <div class="sidebar-widget-list-left">
                              <input type="checkbox" value=""> <a href="#">XXL <span>7</span> </a>
                              <span class="checkmark"></span>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="sidebar-widget pt-40 mt-40 shop-sidebar-border">
                  <h4 class="sidebar-title">Popular Tags </h4>
                  <div class="sidebar-widget-tag mt-20">
                     <ul>
                        <li><a href="#">Clothing</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">For Men</a></li>
                        <li><a href="#">Women</a></li>
                        <li><a href="#">Fashion</a></li>
                     </ul>
                  </div>
               </div> --}}
            </div>
         </div>
         <div class="col-lg-9">
            <div class="shop-topbar-wrapper">
               <div class="shop-topbar-left">
                  <div class="view-mode nav">
                     <a class="active" href="#shop-1" data-toggle="tab"><i class="la la-th"></i></a>
                     <a href="#shop-2" data-toggle="tab"><i class="la la-list-ul"></i></a>
                  </div>
                  {{-- <p>Showing 1 - 20 of 30 results </p> --}}
               </div>
               <div class="product-sorting-wrapper">
                  <div class="product-shorting shorting-style">
                     {{-- <label>View:</label>
                     <select>
                        <option value=""> 20</option>
                        <option value=""> 23</option>
                        <option value=""> 30</option>
                     </select> --}}
                  </div>
                  <div class="product-show shorting-style">
                     <label>Sort by:</label>
                     <select id="sort">
                        <option value="created">Relevance</option>
                                <option value="name-asc">Name, A to Z</option>
                                <option value="name-desc">Name, Z to A</option>
                                <option value="low-high">Price, low to high</option>
                                <option value="high-low">Price, high to low</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="shop-bottom-area">
               <div class="tab-content jump" id="productCat">
                  <div id="shop-1" class="tab-pane active">
                     <div class="row">
                        <?php $count = 0; ?>
                        @if($searched_products !=null)
                        @foreach($searched_products as $product)
                        <?php
                        $count++;
                        $discount = $product->mrp - $product->sell_price;

                        if($discount>0)
                           $discount = ($discount*100)/$product->mrp;
                        ?>
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                           <div class="product-wrap product-border-1 product-img-zoom mb-15">
                              <div class="product-img">
                                 <a href="/product-detailw/{{$product->slug}}"><img src="{{url('/'.$product->image1)}}" alt="product"></a>
                                 <span class="price-dec">-{{round($discount,1)}}%</span>
                                 <div class="product-action-2">
                                    <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                                    <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                                    <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                                 </div>
                              </div>
                              <div class="product-content product-content-padding">
                                 <h4><a href="/product-detailw/{{$product->slug}}">{{substr($product->name,0,18)}}.</a></h4>
                                 <div class="price-addtocart">
                                    <div class="product-price">
                                       <span>₹{{$product->sell_price}}.00</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endforeach
                         
                        @endif                        
                     </div>
                     
                    </div>
                  <div id="shop-2" class="tab-pane">

                     <?php $count=0; ?>
                     @if($searched_products)
                     @foreach($searched_products as $product)
<?php $count++; ?>
                     <div class="shop-list-wrap mb-30">
                        <div class="row">
                           <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6">
                              <div class="product-list-img">
                                 <a href="/product-detail/{{$product->slug}}">
                                    <img src="{{url('/'.$product->image1)}}" alt="Product Style">
                                 </a>
                                 <div class="product-list-quickview">
                                    <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-plus"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xl-8 col-lg-7 col-md-6 col-sm-6">
                              <div class="shop-list-content">
                                 {{-- <span>Chair</span> --}}
                                 <h4><a href="/product-detail//{{$product->slug}}">{{substr($product->name,0,18)}}.</a></h4>
                                 <div class="pro-list-price">
                                    <span>₹{{$product->sell_price}}.00</span>
                                    <span class="old-price">₹{{$product->mrp}}.00</span>
                                 </div>
                                 <p>@if($product->short_descriptions!=null){{strtoupper($product->short_descriptions)}}@endif</p>
                                 <div class="product-list-action">
                                    <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                                    <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-shopping-cart"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                  @endforeach
                  @endif
                     
                  </div>
                     
                  {{-- <div class="pagination-style text-center">
                     <ul>
                        <li><a class="prev" href="#"><i class="la la-angle-left"></i></a></li>
                        <li><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a class="active" href="#">03</a></li>
                        <li><a href="#">04</a></li>
                        <li><a href="#">05</a></li>
                        <li><a href="#">06</a></li>
                        <li><a class="next" href="#"><i class="la la-angle-right"></i></a></li>
                     </ul>
                  </div> --}}
               </div>
            </div>
         </div>
         
      </div>
   </div>
</div>


@endsection







@section('script')
<script>

                // Add to Cart
                function addToCart(id){
                  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                  var quantity = 1;

                  $.ajax({
                     url: '/add-to-cart',
                     type: 'POST',
                     data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
                     success: function (data) {
                      $('#cartItem').html(data.cartItem)
                      $('#cartItem2').html(data.cartItem)
                   },
                   failure: function (data) {
                    Swal(data.message);
                 }
              });
               }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
     }
     else{

       $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
        },
        failure: function (data) {
           Swal(data.message);
        }
     });
    }
 }

 $('.mainCategory').click(function(){
  var categoryId = $(this).attr("id");
  var categoryArray = categoryId.split("_");
  var cat = categoryArray[1];

  var min = $('#min-price').val();
  var max = $('#max-price').val();
  $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/productsCat',
     data:{ cat_id:cat},

     success:function(response){
      $('#productCat').html(response);
      $('#category').empty();

   }
});
});


 $('#sort').on('change',(function(){
    var sort = $('#sort').val();
    var category_id = $('#category').val();
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    var color_code = $('#pro-color').val();
    var size_code = $('#pro-size').val();
    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/sort',
     data:{ sort_type:sort, cat_id :category_id,color: color_code, size: size_code, min_cost: min, max_cost: max},

     success:function(response){
      $('#productCat').html(response);
      $('#min-price').val(min);
      $('#max-price').val(max);
      $('#pro-color').val(color_code);
   }
});
 }));

 $('.colorOnClick').on('click',(function(){
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    var color_code = $(this).attr("id");
    var size_code = $('#pro-size').val();
    var category_id = $('#category').val();
    var val = $('#slider-range').slider("option", "value");

    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/colored',
     data:{ color: color_code, cat_id: category_id, min_cost: min, max_cost: max, size: size_code},
     success:function(response){
      $('#productCat').html(response);            
      $('#pro-color').val(color_code);            
   }
});
 }));


 function priceFilter(){
    var min = $('#min-price').val();
    var max = $('#max-price').val();    
    var color_code = $('#pro-color').val();
    var size_code = $('#pro-size').val();
    var category_id = $('#category').val();
    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/price-filter',
     data:{ max_cost:max , min_cost:min, cat_id: category_id,color: color_code,size: size_code},

     success:function(response){
      $('#productCat').html(response);
   }
});
 }

 $('#size').on('change',(function(){
    var size_code = $('#size').val();
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    var color_code = $('#pro-color').val();
    var category_id = $('#category').val();

    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/size',
     data:{ size: size_code, cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
     success:function(response){
      $('#productCat').html(response);
      $('#cat_id').val(category_id);
      $('#pro-size').val(size_code);
      $('#pro-color').val(color_code);
      console.log(response);
   }
});
 }));

 $('#sort').on('change',(function(){
   var sort = $('#sort').val();
   alert(sort);
   var size_code = $('#size').val();
   var min = $('#min-price').val();
   var max = $('#max-price').val();
   var color_code = $('#pro-color').val();
   var category_id = $('#category').val();

   $.ajax({
      type: 'get',
      data_type: 'html',
      url: '/search-product/sort',
      data:{ size: size_code, sort_type: sort, cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
      success:function(response){
         $('#productCat').html(response);
         $('#cat_id').val(category_id);
         $('#pro-size').val(size_code);
         $('#pro-color').val(color_code);
         console.log(response);
      }
   });
}));

function getRange(){
  var amount = $('#amount').val();
  var amountArray = amount.split(" - ");
  var str1 = amountArray[0];
  str1 = str1.substring(1, 6);
  $('#min-price').val(str1);
 
  var str2 = amountArray[1];
  str2 = str2.substring(1, 6);
  $('#max-price').val(str2);

  priceFilter();
 
}
</script>

@endsection
