<?php
// $contact = explode(',',$companyInfo->contact_no);
?>

@extends('layouts.fyc')

@section('content')

<div class="contact-area pt-85 pb-90">
    <div class="container">
        <div class="contact-info-wrap mb-50">
           @if($companyInfo)
           <h3>contact info</h3>
           <p><?php echo $companyInfo->sort_discription; ?></p>
           <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="single-contact-info text-center mb-30" style="text-align: center;">
                    <i class="ti-location-pin"></i>
                    <h4>our address</h4>
                    <p><?php echo $companyInfo->address; ?></p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="single-contact-info extra-contact-info text-center mb-30">
                    <i class="ti-mobile"></i>
                    <h4>Phone </h4>
                    <ul>
                     
                        <li><i class="ti-mobile"></i>
                            <a href="tel:9958598813">+91-9958598813</a>, <a href="tel:9953979085">+91-9953979085</a>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="single-contact-info text-center mb-30">
                    <i class=" ti-email"></i>
                    <h4>Mail </h4>
                    <ul>
                        <li>
                            <i class="ti-email"></i> <a href="mailto:info@fycprofessional.com"> info@fycprofessional.com</a>
                            <br>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="get-in-touch-wrap">
        <h3>Get In Touch</h3>
        <div class="contact-from contact-shadow">
            <form id="contact-form" method="post" action="{{ url('contact') }}">
             {{ csrf_field() }}
             <div class="row">
                <div class="col-lg-6">
                    <input name="name" id="name" class="input-field" type="text" placeholder="Name">
                </div>
                <div class="col-lg-6">
                    <input name="email" id="email" class="input-field" type="email" placeholder="Email">
                </div>
                <div class="col-lg-12">
                    <select name="subject" id="subject" class="input-field" placeholder="Subject">
                        <option> -- Select Subject --  </option>
                        <option value="Feedback Suggestion">Feedback Suggestion</option>
                        <option value="Become Partner">Become Partner</option>
                        <!-- <option value="Sell on Homeglare">Sell on Homeglare</option> -->
                        <option value="Sponsorship">Sponsorship</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <textarea name="message_content" id="message_content" class="input-field" placeholder="Your Message"></textarea>
                </div>
                <div class="col-lg-12">
                    <button class="submit" type="submit">Send Message</button>
                </div>
            </div>
        </form>
        <p class="form-messege"></p>
    </div>
</div>
<div class="contact-map pt-90">
   <div class="embed-responsive  embed-responsive-21by9">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13997.00498985874!2d77.0883702258456!3d28.712036537625778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d06ac618584fb%3A0x356e84758865f95b!2sSector%204%2C%20Rohini%2C%20Delhi!5e0!3m2!1sen!2sin!4v1580811613376!5m2!1sen!2sin" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
     <!--  <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3496.2752839091822!2d77.04721801441143!3d28.800869283194373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390da82e15c98b6f%3A0x9d5008ce4d6d2ca2!2sYAVI+COSMETICS!5e0!3m2!1sen!2sin!4v1557060301709!5m2!1sen!2sin" frameborder="0" style="border:0; "></iframe> -->
  </div>
  

</div>
</div>
</div>


@endsection