@extends('layouts.fyc')

@section('content')

 <input type="text" name="category" hidden id="category" value="{{$slug}}">
    <input type="text" name="min-price" hidden id="min-price" value="0">
    <input type="text" name="max-price" hidden id="max-price" value="{{$max}}">
<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-5 category-div">
			<select class="form-control" id="cat_products">
				<option>Select Category</option>
				@if(!empty($categories))
				@foreach($categories as $category)
				<option value="{{$category->slug}}" >{{ucfirst($category->category_name)}}</option>
				@endforeach
				@endif				
			</select>
		</div>
		<div class="col-sm-5 price-div">
			<div class="shop-price-filter mt-35 shop-sidebar-border pt-40 sidebar-widget">
				<h4 class="sidebar-title" onclick="showFilter()">Price Filter</h4>
				<div class="price-filter mt-20">
					{{-- <span>Range:  $100.00 - 1.300.00 </span> --}}
					<div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" ><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 66.6667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 66.6667%;"></span></div>
					<div class="price-slider-amount">
						<div class="label-input">
							<input type="text" id="amount" name="price" placeholder="Add Your Price" >
						</div>
						<button type="button" onclick="getRange()">Filter</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="row products-div" id="productCat">
				@php $count=0 @endphp
				@if(!empty($searched_products))
				@foreach($searched_products as $product)
				@php $count=1 @endphp
				
				<div class="cards col-sm-4 col-md-4">
					<div class="card-content">
						<div class="icon-div">
							<i class='far fa-heart' onclick="addToWishlist({{$product->id}})"></i>
							<a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->id}}"><i class='fas fa-eye' onclick="quickView({{$product->id}})"></i></a>
						</div>
						<a href="/product-detail/{{$product->slug}}"><img src="{{asset($product->image1)}}" style="height: auto; width: 100%"></a>
						<div class="card-text">
							<p class="product-name">{{ucfirst(substr($product->name,0,20))}}</p>
							<p class="product-cost">Rs. {{$product->sell_price}}</p>

						</div>
						<div class="add-cart-btn">
							<button class="cart-btn" onclick="addToCart({{$product->id}})">Add to Cart</button>
						</div>
					</div>
				</div>
				@endforeach
				@endif
				
				@if($count == 0)
				<div class="container" style="text-align: center; padding: 50px"><h3>No result found</h3></div>
				@endif
				
			</div>
		</div>
		<div class="col-sm-1"></div>
		
	</div>
</div>


@endsection


@section('script')

<script>
	function showFilter(){
		$('.price-filter').toggle();	
	}




	              // Add to Cart
                function addToCart(id){
                  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                  var quantity = 1;

                  $.ajax({
                     url: '/add-to-cart',
                     type: 'POST',
                     data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
                     success: function (data) {
                      $('#cartItem').html(data.cartItem)
                      $('#cartItem2').html(data.cartItem)
                   },
                   failure: function (data) {
                    Swal(data.message);
                 }
              });
               }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
     }
     else{

       $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
        },
        failure: function (data) {
           Swal(data.message);
        }
     });
    }
 }



 function priceFilter(){
    var min = $('#min-price').val();
    var max = $('#max-price').val();    
    var category_id = $('#category').val();
    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/price-filter',
     data:{ max_cost:max , min_cost:min, cat_id: category_id},

     success:function(response){
      $('#productCat').html(response);
		$('.price-filter').hide();	

   }
});
 }


 function getRange(){
  var amount = $('#amount').val();
  var amountArray = amount.split(" - ");
  var str1 = amountArray[0];
  str1 = str1.substring(1, 6);
  $('#min-price').val(str1);
 
  var str2 = amountArray[1];
  str2 = str2.substring(1, 6);
  $('#max-price').val(str2);

  priceFilter();
 
}

$('#cat_products').on('change',function(){
	var slug = $('#cat_products').val();

	window.location.href = '/products/'+slug;
});

	
</script>

@endsection