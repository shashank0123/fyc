@extends('layouts.fyc')

@section('content')
@if(!empty($banners))
@php  $i=0 @endphp
<!-- Slider Section -->
<div class="slider-area">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">

         <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
         <!-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> -->
      </ol>
      <div class="carousel-inner">
         @foreach($banners as $banner)
         <div class="carousel-item @if($i==0){{'active'}}@endif">
            <img class="d-block w-100" src="{{url($banner->image_url)}}" alt="First slide">
         </div>
         @php  $i++ @endphp
         @endforeach
         <!-- <div class="carousel-item">
            <img class="d-block w-100" src="{{url('fyc/slider2.jpg')}}" alt="Second slide">
         </div> -->
         <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/slider3.jpg" alt="Third slide">
         </div>
         <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/D.jpg" alt="Third slide">
         </div>  -->
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
      </a>
   </div>
</div>

@endif

<br><br>

<!-- Multi Row Product Slider -->
<div class="product-div">
   <h1>Products</h1>
   <a href="{{url('products/all')}}" class="btn btn-primary product-head">View All</a>
   <div class="row carouseled">
      @if(!empty($products))
      @foreach($products as $product)
     <div class="cards col-sm-12 col-md-12">
      <div class="card-content">
         <div class="icon-div">
            <i class='far fa-heart' onclick="addToWishlist({{$product->id}})"></i>
            <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->id}}"><i class='fas fa-eye' onclick="quickView({{$product->id}})"></i></a>
         </div>
         <a href="/product-detail/{{$product->slug}}" ><img src="{{asset($product->image1)}}" style="height: auto; width: 100%"></a>
         <div class="card-text">
            <p class="product-name">{{ucfirst(substr($product->name,0,20))}}</p>
            <p class="product-cost">Rs. {{$product->sell_price}}</p>
            
         </div>
         <div class="add-cart-btn">
            <button class="cart-btn" onclick="addToCart({{$product->id}})">Add to Cart</button>
         </div>
      </div>
   </div>
   @endforeach
   @endif
  
</div>
</div>

<br><br>

@if(!empty($medium_banner))
<div class="image-div">
   <img src="{{asset($medium_banner->image_url)}}">
</div>
@endif

<br><br><br>
<div class="video-div">
  <br>
<div class="container-fluid">
  <div class="row">
  <div class="col-sm-6">
<iframe src="https://www.youtube.com/embed/wichH9uPXXs" width="100%" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>
   <div class="col-sm-6">
    <iframe src="https://www.youtube.com/embed/QVrA2UqZ-So" width="100%" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>
</div>
</div>
<br>
</div>






<br><br><br>

@if(!empty($full_banners))
@php  $i=0 @endphp
<!-- Slider Section -->
<div class="slider-area">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">

         <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
         <!-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> -->
      </ol>
      <div class="carousel-inner" >
         @foreach($full_banners as $banner)
         <div class="carousel-item @if($i==0){{'active'}}@endif">
            <img class="d-block w-100" src="{{url($banner->image_url)}}" alt="First slide">
         </div>
         @php  $i++ @endphp
         @endforeach
         <!-- <div class="carousel-item">
            <img class="d-block w-100" src="{{url('fyc/slider2.jpg')}}" alt="Second slide">
         </div> -->
         <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/slider3.jpg" alt="Third slide">
         </div>
         <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/D.jpg" alt="Third slide">
         </div>  -->
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
      </a>
   </div>
</div>

@endif

<br><br>


<!-- Single Row Product Slider -->

<div class="product-div">

   <h1>Our Best Sellers</h1>
   <a href="{{url('products/trending')}}" class="btn btn-primary product-head">View All</a>
   <div class="row carouseled-2">
      @if(!empty($best_products))
      @foreach($best_products as $product)
     <div class="cards col-sm-12 col-md-12">
      <div class="card-content">
         <div class="icon-div">
            <i class='far fa-heart' onclick="addToWishlist({{$product->id}})"></i>
            <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->id}}"><i class='fas fa-eye' onclick="quickView({{$product->id}})"></i></a>
         </div>
         <a href="/product-detail/{{$product->slug}}" ><img src="{{asset($product->image1)}}" style="height: auto; width: 100%"></a>
         <div class="card-text">
            <p class="product-name">{{ucfirst(substr($product->name,0,20))}}</p>
            <p class="product-cost">Rs. {{$product->sell_price}}</p>
            
         </div>
         <div class="add-cart-btn">
            <button class="cart-btn" onclick="addToCart({{$product->id}})">Add to Cart</button>
         </div>
      </div>
   </div>
   @endforeach
   @endif
   
</div>
</div>


<br><br><br>


<!-- Testimonial Section -->
<div class="testimonial-section" >

   <h1>Client Testimonials </h1>
   <h6>What they say</h6>

   <div class="hr-div">
      _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ <img src="/fyc/leaf.png"> _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
   </div>

   <div class="container test-div">
     <div class="row">
       <div class="col-md-12">
         <div class="carousel slide" data-ride="carousel" id="quote-carousel">

            <!-- Bottom Carousel Indicators -->
            <ol class="carousel-indicators">
              @if(!empty($testimonials))
            @php $i=0  @endphp
            @foreach($testimonials as $test)
              <li data-target="#quote-carousel" data-slide-to="{{$i}}" class="@if($i==0){{'active'}}@endif"></li>
              @php $i++  @endphp
              @endforeach
              @endif
           </ol>

           <!-- Carousel Slides / Quotes -->
           <div class="carousel-inner">
            @if(!empty($testimonials))
            @php $i=0  @endphp
            @foreach($testimonials as $test)
            <!-- Quote 1 -->
            <div class="item @if($i==0){{'active'}}@endif">
              <div class="row">
                <div class="col-sm-12">
                  <p>&ldquo;{{$test->review}}&rdquo;</p>
                  <img src="{{asset($test->image_url)}}" class="testimonial-img"><br>
                  <small><strong class="test-text">{{$test->client_name}}</strong><br> {{$test->city}}</small>
               </div>
            </div>
         </div>
         @php $i++  @endphp
         @endforeach
         @endif

</div>


</div>                          
</div>
</div>
</div>


</div>

@endsection


@section('script')

<script>


   $(document).ready(function() {
  //carousel options
  $('#quote-carousel').carousel({
    pause: true, interval: 10000,
 });
});

  // Add to Cart
    function addToCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // Swal(id);
        var quantity = 1;

      $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // Swal(data.cartItem);
           $('#cartItem').html(data.cartItem);
           $('#cartItem2').html(data.cartItem);
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
   }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
      }
      else{
        // Swal(id);

      $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // Swal(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
         },
         failure: function (data) {
           Swal(data);
         }
      });
      }
   }

</script>

@endsection