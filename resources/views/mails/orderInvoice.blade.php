<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome To Fyc Professional</title>
<style>
.box
{
        height: 63px;
    border: 1px solid #000;
    background: black;
    color: #fff;
    line-height: 32px;
    margin-left: 15%;
    margin-right: 15%;
    padding-left: 11%;
    border-radius: 10px
}
.small-box
{
    height: 29px;
    border: 1px solid #000;
    background: black;
    color: #fff;
    line-height: 28px;
    margin-left: 42%;
    margin-right: 15%;
    padding-left: 5%;
    border-radius: 10px;
    width: 44%;
}
.snapBox
{
        height: 100px;
    border: 1px solid #000;
    width: 45%;
}
</style>
</head>

<body>
<!-- <div width="512" style="text-align:center;">
    <a href="#">Can't see our images? Click here.</a>
</div> -->
<table align="center" border="0" cellspacing="0" cellpadding="0" width="512" style="border:1px solid #e8e8e8;">
    <tbody>
        <tr><td colspan="3" height="10"></td></tr>
        <tr>
            <td width="10"></td>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="490">
                    <tbody>
                        <tr>
                          <td colspan="3" height="70" align="center" style="background-color:#CFD9EB">
                            <a href="{{ url('/') }}"> <img src="{{ asset('/fyc-new/images/logo/logo.png') }}" width="100" height="65"></a>
                          </td>
                        </tr>
                        <tr><td colspan="3" height="10"></td></tr>
                        <tr>
                            <td width="10"></td>
                            <td>
                              @if($user_type =='admin')

                                <p>A new user booked order having detail :</p>
                                <p>Name : {{ $user->name ?? ''}} </p>
                                <p>Email : {{ $user->email ?? '' }}</p>

                              @else

                                <p>Dear {{ $user->name }}, your order has been booked </p>

                              @endif
                                <h2>Order Details </h2>
                                <p>Order Id : {{$order->id}} | Total Price : {{$order->price}} </p>
                                <br>
                            </td>
                            <td width="10"></td>
                        </tr>
                        <tr>
                            <td width="10"></td>
                            <td>
                                <br>
                            </td>
                            <td width="10"></td>
                        </tr>
                        <tr>
                            <td width="10"></td>
                            <td>
                              <table style="width:100%">
                               <caption><h3>Product Details</h3></caption>
                               <tr>
                                 <th>Product Name</th>
                                 <th>Quantity</th>
                                 <th>Price</th>
                               </tr>
                               @foreach($productDetails as $product)
                               <tr>
                                 <td>{{$product->name}}</td>
                                 <td>{{$product->quantity}}</td>
                                 <td>Rs. {{$product->price}}</td>
                               </tr>
                               @endforeach
                              </table>
                            </td>
                            <td width="10"></td>
                        </tr>
                        <tr><td colspan="3" height="10"></td></tr>
                        <tr><td colspan="3" height="30" style="background-color:#e06f47;"></td></tr>
                    </tbody>
                </table>
            </td>
            <td width="10"></td>
        </tr>
        <tr><td colspan="3" height="10"></td></tr>
    </tbody>
</table>
</body>
</html>
