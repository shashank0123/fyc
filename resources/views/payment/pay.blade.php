
<?php 
$product_name = "Product Purchase";
$price = $amount;
$name = $user->name;
$phone = $phone;
$email = $user->email;
       //Download from website
$api = new Instamojo\Instamojo('YOU_PRIVATE_API_KEY', 'YOUR_PRIVATE_AUTH_TOKEN','https://test.instamojo.com/api/1.1/');
try {
    $response = $api->paymentRequestCreate(array(
        "purpose" => $product_name,
        "amount" => $price,
        "buyer_name" => $name,
        "phone" => $phone,
        "send_email" => true,
        "send_sms" => true,
        "email" => $email,
        'allow_repeated_payments' => false,
        "redirect_url" => "http://homeglare.com/thankyou",
        "webhook" => "http://homeglare.com/webhook"
        ));
    //print_r($response);
    $pay_url = $response['longurl'];
    
    header("Location: $pay_url");
    exit();
}
catch (Exception $e) {
    print('Error: ' . $e->getMessage());
}     
?>