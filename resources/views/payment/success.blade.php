@extends('layouts.fyc')

@section('content')

<div class="container">
	<div style="text-align: center;">

		<h1 style="text-align: center; padding: 5% 0 0; font-size: 42px">
			Order Booked Successfully.
		</h1>
		<p>With</p>
		<h4>Transaction ID : {{$txnid}}</h4>
		<p>of</p>
		<h4>Amount : {{$amount}}</h4>
		<p>Mobile : {{$phone}}</p>
	</div>
	<div class="row">
	    <table class="table">
	        
	        <thead>
	            <tr>
	                <th colspan='2'># Products</th>
	                <th>Quantity</th>
	                <th>Price</th>
	            </tr>
	        </thead>
	        
	        <tbody>
	            @foreach($products as $product)
	            
	            <tr>
	                <td>
	                    <a href="{{ url('product-detail/'.$product->slug ?? '') }}"><img src="{{ asset($product->image1) }}" style="height: 100px;"></a>
	                </td>
	                <td><a href="{{ url('product-detail/'.$product->slug ?? '') }}">{{ $product->name ?? '' }}</a></td>
	                <td>{{ $product->quantity ?? '0' }}</td>
	                <td>{{ $product->price ?? '0' }}</td>
	            </tr>
	            
	            @endforeach
	            <tr></tr>
	        </tbody>
	        
	    </table>
	</div>
</div>

@endsection