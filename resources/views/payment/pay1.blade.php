<?php $amount = $amount*100;
?>
@extends('layouts.homeglare')

@section('content')
<style type="text/css"> 
	.razorpay-payment-button { display: none; }
</style>
<main style="height: 500px">
{{-- <button type="hidden" id="sample" onclick="clickMe()">Ok</button> --}}
<form action="charge.php" method="POST">
    <!-- Note that the amount is in paise = 50 INR -->
    <script
        src="http://checkout.razorpay.com/v1/checkout.js"
        data-key="rzp_test_2CJezTpsnYVZuZ"
        data-amount="<?php echo $amount ?>"
        data-phone="<?php echo $phone; ?>"
        data-buttontext="Pay with Razorpay"
        data-name="Homeglare.com"
        data-description="Test Txn with RazorPay"
        data-image="http://homeglare.com/fyc-new/images/logo/logo.png"
        data-prefill.name="<?php echo $user->name; ?>"
        data-prefill.email="<?php echo $user->email; ?>"
        {{-- data-theme.color="#F37254" --}}
        data-theme.color="#000033"
    ></script>
    <input type="hidden" value="Hidden Element" name="hidden">
    </form>
</main>
@endsection

@section('script')
  
<script type="text/javascript">


	$("document").ready(function() {
    setTimeout(function() {
        $(".razorpay-payment-button").trigger('click');
    },10);
});


	// function clickMe(){
	// 	$('.razorpay-payment-button').click();
	// }
 </script>

@endsection

