@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                                pattern="^[a-zA-Z\s]*$" onkeyup="checkInputValue(this)" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" maxlength="10" class="form-control @error('phone') is-invalid @enderror" name="phone"
                                value="{{ old('phone') }}" pattern="^\d+$" onkeyup="checkInputValue(this)" required autocomplete="phone" autofocus>
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"  name="email"
                                value="{{ old('email') }}" pattern="^.+@[^\.].*\.[a-z]{2,}$" onkeyup="checkInputValue(this)" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address"
                                 value="{{ old('address') }}" required autocomplete="address">
                                 {{ old('address') }}</textarea>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 text-center offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('REGISTER') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

/* Start input check regex */
  function checkInputValue(field) {
    var value = field.value;
    var fieldId = field.id;
    var pattern = field.pattern;
    var pattern = new RegExp(field.pattern);
    var re = pattern.test(value);
    // var re = value.match(pattern);
    if(!re) {
        $('#' + fieldId + '_error').removeClass('d-none');
        if (document.getElementById(fieldId + '_error')) {
          $('#' + fieldId + '_error').removeClass('d-none');
          $('.btn-submit').prop('disabled', true);
        } else {
            $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
            $('.btn-submit').prop('disabled', true);
        }
    } else {
        $('#' + fieldId + '_error').addClass('d-none');
        $('.btn-submit').prop('disabled', false)
    }
  };
/* End input check regex*/
</script>

@endsection
