@extends('layouts.app')

@section('content')





@if($message = Session::get('message'))
   <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif

     
          <div class="login-register-area pt-85 pb-90">
            <div class="container">
              <div class="row">
                <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                  <div class="login-register-wrapper">
                    <div class="login-register-tab-list nav">
                      <a class="active" data-toggle="tab" href="#lg1">
                        <h4> login </h4>
                      </a>
                      <a data-toggle="tab" href="#lg2">
                        <h4> register </h4>
                      </a>
                    </div>
                    <div class="tab-content">
                      <div id="lg1" class="tab-pane active">
                        <div class="login-form-container">
                          <div class="login-register-form">
                            <form method="POST" action="/login<?php if(!empty($_GET['page']))echo'?page=checkout'; ?>"> 
                              @csrf
                              <input id="email" type="email" class="input-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                              @error('email')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror

                              <input id="password" type="password" class="input-field @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                              @error('password')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror

                              <div class="button-box">
                                <div class="login-toggle-btn">

                                  <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                  <label for="remember">
                                    {{ __('Remember Me') }}
                                  </label>                                                       
                                  @if (Route::has('password.request'))
                                  <a class="" href="{{ route('password.request') }}">
                                    {{ __('Forgot Password?') }}
                                  </a>
                                  @endif

                                </div>
                                <button type="submit">Login</button>
                              </div>
                              <hr>
                              <!-- <div style="text-align: center;">OR</div>
                              <div class="form-group row mb-0">
                               <div class="col-md-12 row">
                                <div class="col-md-6">
                                  <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-primary text-fb" style="width: 100%"><i class="fa fa-facebook"></i> Facebook</a>
                                </div>
                                <div class="col-md-6">                              
                                  <a href="{{url('/redirect/google')}}" class="btn btn-danger" style="width: 100%"><i class="fa fa-google"></i> Google</a>
                                </div>
                              </div>
                            </div> -->
                          </form>
                        </div>
                      </div>
                    </div>
                    <div id="lg2" class="tab-pane">
                      <div class="login-form-container">
                        <div class="login-register-form">
                        <form method="POST" action="{{ route('register') }}">
                        @csrf
                          {{-- <form action="#" method="post"> --}}

                            <input id="name" type="text" class="input-field @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                            pattern="^[a-zA-Z\s]*$" onkeyup="checkInputValue(this)" required autocomplete="name" autofocus  placeholder="Username">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                            {{-- <input type="text" name="user-name" placeholder="Username" class="input-field"> --}}


                            <input id="phone" type="text" maxlength="10" class="input-field @error('phone') is-invalid @enderror" name="phone"
                            value="{{ old('phone') }}" pattern="^\d+$" onkeyup="checkInputValue(this)" required autocomplete="phone" autofocus placeholder="Phone">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                            {{-- <input type="password" name="user-password" placeholder="Password" class="input-field"> --}}

                            <input id="email" type="email" class="input-field @error('email') is-invalid @enderror"  name="email"
                            value="{{ old('email') }}" pattern="^.+@[^\.].*\.[a-z]{2,}$" onkeyup="checkInputValue(this)" required autocomplete="email" placeholder="Email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                            {{-- <input name="user-email" placeholder="Email" type="email" class="input-field"> --}}

                            <textarea id="address" type="text" placeholder="Complete Address" class="textarea @error('address') is-invalid @enderror" name="address"
                            required ></textarea>

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror


                            <input id="password" type="password" class="input-field @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
                            <h6 style="margin-top: -25px; color: blue">(Minimum 8 characters are required.)</h6>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror 


                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">

                            <div class="button-box">
                              <button type="submit">Register</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>















{{-- 

          <div class="container ">
            <div class="row justify-content-center">
              <div class="col-md-8">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link btn btn-outline-dark active" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true">Login</a>
                  </li>
                  <li class="nav-item mx-2">
                    <a class="nav-link btn btn-outline-dark" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false">Register</a>
                  </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                  <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                    <div class="card">
                      <div class="card-header text-white bg-00BB00">{{ __('Login') }}</div>

                      <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                          @csrf

                          <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                              @error('email')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                              @error('password')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                            </div>
                          </div>

                          <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                  {{ __('Remember Me') }}
                                </label>
                              </div>
                            </div>
                          </div>

                          <div class="form-group row mb-0">
                            <div class="col-md-8  offset-md-4">
                              <button type="submit" class="btn btn-success bg-00BB00">
                                {{ __('LOGIN') }}
                              </button>

                              @if (Route::has('password.request'))
                              <a class="btn btn-success bg-00BB00" href="{{ route('password.request') }}">
                                {{ __('Forgot Password?') }}
                              </a>
                              @endif
                            </div>
                          </div>
                          <hr>
                          <div class="form-group row mb-0">
                           <div class="col-md-8  offset-md-4">
                            <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-success text-fb"><i class="fa fa-facebook"></i> Facebook</a>
                            <a href="{{url('/redirect/google')}}" class="btn btn-success"></a>
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
                  <div class="card">
                    <div class="card-header text-white bg-00BB00">{{ __('Register') }}</div>

                    <div class="card-body">
                      <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                          <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                          <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                            pattern="^[a-zA-Z\s]*$" onkeyup="checkInputValue(this)" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                          <div class="col-md-6">
                            <input id="phone" type="text" maxlength="10" class="form-control @error('phone') is-invalid @enderror" name="phone"
                            value="{{ old('phone') }}" pattern="^\d+$" onkeyup="checkInputValue(this)" required autocomplete="phone" autofocus>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                          <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"  name="email"
                            value="{{ old('email') }}" pattern="^.+@[^\.].*\.[a-z]{2,}$" onkeyup="checkInputValue(this)" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                          <div class="col-md-6">
                            <textarea id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address"
                            value="{{ old('address') }}" required autocomplete="address">
                          {{ old('address') }}</textarea>

                          @error('address')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                          @error('password')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                      </div>

                      <div class="form-group row mb-0">
                        <div class="col-md-6 text-center offset-md-4">
                          <button type="submit" class="btn btn-success bg-00BB00">
                            {{ __('REGISTER') }}
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> --}}


      @endsection

      @section('script')
      <script type="text/javascript">

        /* Start input check regex */
        function checkInputValue(field) {
          var value = field.value;
          var fieldId = field.id;
          var pattern = field.pattern;
          var pattern = new RegExp(field.pattern);
          var re = pattern.test(value);
    // var re = value.match(pattern);
    if(!re) {
      $('#' + fieldId + '_error').removeClass('d-none');
      if (document.getElementById(fieldId + '_error')) {
        $('#' + fieldId + '_error').removeClass('d-none');
        $('.btn-submit').prop('disabled', true);
      } else {
        $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
        $('.btn-submit').prop('disabled', true);
      }
    } else {
      $('#' + fieldId + '_error').addClass('d-none');
      $('.btn-submit').prop('disabled', false)
    }
  };
  /* End input check regex*/
</script>
@endsection
