<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingAddressToAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
          $table->string('ship_to_different_address')->nullable();
          $table->string('shipping_address')->nullable();
          $table->string('shipping_country')->nullable();
          $table->string('shipping_city')->nullable();
          $table->string('shipping_state')->nullable();
          $table->bigInteger('shipping_pin_code')->nullable();
          $table->string('shipping_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('ship_to_different_address');
            $table->dropColumn('shipping_address');
            $table->dropColumn('shipping_country');
            $table->dropColumn('shipping_city');
            $table->dropColumn('shipping_state');
            $table->dropColumn('shipping_pin_code');
            $table->dropColumn('shipping_phone');
        });
    }
}
