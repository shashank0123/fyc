<?php
 namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
use Log;

class SocialAuthGoogleController extends Controller
{
  /**
   * Create a redirect method to google api.
   *
   * @return void
   */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }
/**
     * Return a callback method from google api.
     *
     * @return callback URL from google
     */
    public function callback()
    {
        // $user = $service->createOrGetUser(Socialite::driver('google')->user());
        // auth()->login($user);
        // return redirect()->to('/');

        // try {

            $user = Socialite::driver('google')->user();
            // Log::info(print_r($user, true));
            $finduser = User::where('provider_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);

                return redirect('/');

            }else{
                // check for email exist
                $userExist = User::where('email', $user->email)->first();
                if($userExist){
                    User::find($userExist->id)->update(['provider' => 'google',
                                                        'provider_id' => $user->id]);
                    Auth::login($userExist);
                } else {
                    $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'provider' => 'google',
                    'provider_id' => $user->id,
                    'access_type' => 'customer',
                    'status' => 'active',
                ]);

                Auth::login($newUser);
                }

                return redirect('/');
            }

        // } catch (Exception $e) {
        //     return redirect('/login');
        // }

    }
}
