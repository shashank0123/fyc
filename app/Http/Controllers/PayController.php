<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\OrderItem;
use App\Models\Order;
use App\User;
 
class PayController extends Controller
{
   
   
   public function pay(Request $request){
 
     $api = new \Instamojo\Instamojo(
            config('services.instamojo.api_key'),
            config('services.instamojo.auth_token'),
            config('services.instamojo.url')
        );
 
    try {
        $response = $api->paymentRequestCreate(array(
            "purpose" => "Product Purchase",
            "amount" => $request->amount,
            "buyer_name" => "$request->name",
            "send_email" => true,
            "email" => "$request->email",
            "phone" => "$request->mobile_number",
            "redirect_url" => "http://fycprofessional.com/pay-success"
            ));
             
            header('Location: ' . $response['longurl']);
            exit();
    }catch (Exception $e) {
        print('Error: ' . $e->getMessage());
    }
 }
 
 public function success(Request $request){

 		$message = 'Something Went Wrong';

 	$data = $request->all();
 	$data = $data['payment_request_id'];
 	
     try {
 
        $api = new \Instamojo\Instamojo(
            config('services.instamojo.api_key'),
            config('services.instamojo.auth_token'),
            config('services.instamojo.url')
        );
 
        $response = $api->paymentRequestStatus(request('payment_request_id'));
 		
        if( !isset($response['payments'][0]['status']) ) {
         return redirect()->back()->with('message',$message);
        } else if($response['payments'][0]['status'] != 'Credit') {
         return redirect()->back()->with('message',$message); 
        } 
        else if($response['payments'][0]['status'] == 'Credit'){
        	$email = $response['email'];

        	$user = User::where('email',$email)->first();
        			
        		$payment = DB::table('orders')->where('user_id',$user->id)->where('status','in_process')->orderBy('created_at','DESC')->first();

        	$txn_id = $response['payments'][0]['payment_id'];
        	
        	$payment->transaction_id = $txn_id;
        	$payment->encrypted_payment_id = $data;
        	$payment->status = 'order_booked';
        		$payment->update();
        		$message = 'Payment success';
        		
        		$orders = $payment;
        		
        		$productDetails = DB::table('order_items')
  ->join('products', 'products.id', '=', 'order_items.item_id')
  ->where('order_items.order_id', $orders->id)
  ->select('products.name', 'order_items.quantity', 'order_items.price')
  ->get();
        		
        		
        		
        		
        		
        		 
  Mail::send('mails.orderInvoice', ['user' => $user, 'order' => $orders, 'productDetails' => $productDetails, 'user_type' => 'admin'],
                 function ($m) use ($user) {
                     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                     $m->to('info@fycprofessional.com', 'Fyc Professional')->subject('Order Invoice');
                    //  $m->to('jyotikasethi3007@gmail.com', $user->name)->subject('Order Invoice');
                 });
  
  
                                   Log::info($productDetails);
                 Mail::send('mails.orderInvoice', ['user' => $user, 'order' => $orders, 'productDetails' => $productDetails, 'user_type' => 'user'],
                 function ($m) use ($user) {
                     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                     
                     $m->to($user->email, $user->name)->subject('Order Invoice');
                 });
        		
        		
        		
        		

        		session()->forget('cart');
        //  return view('fyc-web.ordersuccess')->with('message',$message);
        return redirect('order-booked')->with('success', 'Order Booked Successfully');
        	
        }
      }catch (\Exception $e) {
         return redirect()->back()->with('message',$message);
     }

  }
}