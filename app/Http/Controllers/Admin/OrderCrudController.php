<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OrderRequest as StoreRequest;
use App\Http\Requests\OrderRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/order');
        $this->crud->setEntityNameStrings('order', 'orders');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $this->crud->addFields([
            ['name' => 'shipping_address',  'label' => "Shipping Address",'type' => 'text' ],
            ['name' => 'price','label' => "Price",'type' => 'number' ],
            // ['name' => 'id','label' => "Order Detail",'type' => 'text' ],
            ['name' => 'payment_method','label' => "Payment Type",'type' => 'text'],
            ['name' => 'transaction_id','label' => "Transaction Id",'type' => 'text'],
            ['name' => 'status', 'label' => "Status", 'type' => 'select_from_array', 'options' =>
              ['order_booked' => 'Order Placed', 'processing' => 'Processing', 'preparing' => 'Preparing', 'shiped' => 'Shiped', 'delivered' => 'Delivered']
            ],
        ]);

        $this->crud->addColumns([
            'user_id',
            'shipping_address',
            // ['name' => 'id','label' => "Order Detail",'type' => 'text'],
            'price','payment_method','transaction_id','status']);

        // add asterisk for fields that are required in OrderRequest
        // $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        // add filters
        // $this->crud->filters();
        // $this->crud->addFilter([ // simple filter
        //         'type' => 'text',
        //         'name' => 'address',
        //         'label'=> 'Address'
        //       ],
        //       false,
        //       function($value) { // if the filter is active
        //           $this->crud->addClause('where', 'address', 'LIKE', "%$value%");
        //       } );
        $this->crud->addFilter([ // date filter
          'type' => 'date_range',
          'name' => 'created_at',
          'label'=> 'Date'
        ],
        false,
        function($value) { // if the filter is active, apply these constraints
          $dates = json_decode($value);
          $this->crud->addClause('where', 'created_at', '>=', $dates->from);
          $this->crud->addClause('where', 'created_at', '<=', $dates->to);
          // $this->crud->addClause('where', 'created_at', '=', $value);
        });
        // $this->crud->addFilter([ // dropdown filter
        //   'name' => 'status',
        //   'type' => 'dropdown',
        //   'label'=> 'Status'
        // ], [
        //   1 => 'In stock',
        //   2 => 'In provider stock',
        //   3 => 'Available upon ordering',
        //   4 => 'Not available',
        // ], function($value) { // if the filter is active
        //     $this->crud->addClause('where', 'status', $value);
        // });
        // add export button
        $this->crud->enableExportButtons();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
