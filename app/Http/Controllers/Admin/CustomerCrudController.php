<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CustomerRequest as StoreRequest;
use App\Http\Requests\CustomerRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CustomerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CustomerCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Customer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/customer');
        $this->crud->setEntityNameStrings('customer', 'customers');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addFields([
            ['name' => 'name',  'label' => "Product Name",'type' => 'text' ],
            ['name' => 'email','label' => "Email",'type' => 'text'],
            ['name' => 'access_type', 'label' => "Access Type", 'type' => 'select_from_array',
            'options' => ['customer' => 'Customer', 'admin' => 'Admin']],
            ['name' => 'status', 'label' => "Status", 'type' => 'select_from_array',
            'options' => ['active' => 'Active', 'inactive' => 'Inactive', 'approve' => 'Approve', 'reject' => 'Reject']]

        ]);

        $this->crud->addClause('where', 'access_type', '=', 'customer');

        // add asterisk for fields that are required in CustomerRequest
        // $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // add export button
        $this->crud->enableExportButtons();

        $this->crud->setColumns(['name', 'email', 'access_type', 'status']);

        $this->crud->addColumns(['name', 'email', 'access_type', 'status']);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
