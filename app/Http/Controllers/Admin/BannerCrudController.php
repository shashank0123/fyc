<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BannerRequest as StoreRequest;
use App\Http\Requests\BannerRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class BannerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BannerCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Banner');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/banner');
        $this->crud->setEntityNameStrings('banner', 'banners');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        $this->crud->addFields([
            ['name' => 'image_url',    'label' => 'Image ','type' => 'upload','upload' => true,'disk' => 'uploads' ],            
            ['name' => 'image_type','label' => 'Image Type','type' => 'select_from_array','options' => ['slider' => 'Slider Banner', 'full' => 'Full Screen Banner', 'medium' => 'Medium Size Banner', 'small' => 'Small Size Banner', 'portrait' => 'Portrait Banner'],'allows_null' => true   ],
            ['name' => 'keyword', 'label' => 'Keyword', 'type' => 'text'],      
            ['name' => 'slug', 'label' => 'Slug', 'type' => 'text'],      
            ['name' => 'status', 'label' => "Status", 'type' => 'select_from_array', 'options' => ['Active' => 'Active', 'Deactive' => 'Deactive']], 
            
        ]); 

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

        // add asterisk for fields that are required in BannerRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // To view List of All Banners
        $this->crud->setColumns(['keyword','image_type','image_url' ,'status']);
        
          $this->crud->addColumns([
            [
               'name' => 'image_url', // The db column name
               'label' => "Image Url", // Table column heading
               'type' => 'image',
                // 'prefix' => 'folder/subfolder/',
                // optional width/height if 25px is not ok with you
                'height' => '50px',
                 'width' => '50px',
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
