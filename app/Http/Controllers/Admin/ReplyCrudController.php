<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Models\Help;
use App\Http\Requests\ReplyRequest as StoreRequest;
use App\Http\Requests\ReplyRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Log;

/**
 * Class ReplyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ReplyCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Reply');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/reply');
        $this->crud->setEntityNameStrings('reply', 'replies');

        $this->crud->setRoute(config('backpack.base.route_prefix') . '/reply');

        $this->crud->backlink = config('backpack.base.route_prefix') . '/reply?help_id=' . request('help_id').'&help_id=' . request('help_id');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        if(request()->has('help_token'))
        {
            $this->crud->addField([  // Select2
               'label' => "Token",
               'type' => 'hidden',
               'name' => 'token',
               'value' => request('help_token')
            ]);

            $this->crud->addField([  // Select2
               'label' => "Reply from",
               'type' => 'hidden',
               'name' => 'reply_from',
               'value' => 'admin'
            ]);

            $this->help = Help::first('token', request('help_token'));
            $this->crud->addClause('where', 'token', '=', request('help_token'));
            // $this->crud->setEntityNameStrings(' Help & Support', 'Help & Support');

            $this->crud->headlink = config('backpack.base.route_prefix') . '/help?help_token='.$this->help->token;

            $this->crud->headname = 'all help pages';

        }

        // add asterisk for fields that are required in ReplyRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->removeAllButtons();
        // $this->crud->addButtonFromModelFunction('line', 'editReply', 'editReply', 'beginning');
        // $this->crud->addButtonFromModelFunction('line', 'deleteReply', 'deleteReply', 'end');
        $this->crud->addButtonFromModelFunction('top', 'addReply', 'addReply', 'beginning');

        $this->crud->addFields([
            ['label' => 'Message', 'name' => 'message', 'type' => 'text'],

        ]);


    }

    public function performSaveAction($itemId = null)
    {
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ? $itemId : \Request::input('id');

        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = 'admin/reply/create?help_token=' . $this->crud->entry->help->token;
                break;
            case 'save_and_edit':
                $redirectUrl = 'admin/reply'.'/'.$itemId.'/edit?help_token=' . $this->crud->entry->token;
                if (\Request::has('locale')) {
                    $redirectUrl .= '&locale='.\Request::input('locale');
                }
                break;
            case 'save_and_back':
            // Log::info($this->crud->entry);
                $redirectUrl = 'admin/reply'.'/'.$itemId.'/edit?help_token=' . $this->crud->entry->token;
                if (\Request::has('locale')) {
                    $redirectUrl .= '&locale='.\Request::input('locale');
                }
            default:
                $redirectUrl = 'admin/reply?help_token=' . $this->crud->entry->token;
                break;
        }

        return \Redirect::to($redirectUrl);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
