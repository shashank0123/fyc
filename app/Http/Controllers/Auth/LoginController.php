<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Log, Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function postLogin(Request $request)
    {
      $next = session()->get('url.intended');
		
		$data = $request->all();

		if ( \Auth::attempt([
			'email' => $request->email,
			'password' => $request->password
		],$request->has('remember'))){
			if(session()->has('url.intended'))
			{
				echo $next = session()->get('url.intended');
			}
			
			if(!empty($_GET['page'])){
			    $userId = Auth::user()->id;
			    	return redirect('/checkout/'.$userId);
			}
			else{
			    	return redirect('/');
			}
			
		
			return redirect($next);
		}
		else{

			return redirect('login')->with(
				'fail', '1'
			);
		}

  }
}
