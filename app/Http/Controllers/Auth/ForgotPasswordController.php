<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Mail;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function sendEmail(Request $request){
        
        $user= User::where('email',$request->email)->first();
        
        
        
        
        if(!empty($user)){
            $link = 'reset-password/'.$user->id;
            
            Mail::send('mails.forgotPassword', ['user' => $user, 'link' => $link],
                 function ($m) use ($user) {
                     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                     $m->to($user->email, $user->name ?? '')->subject('Reset Password');
                    //  $m->to('jyotikasethi3007@gmail.com', $user->name)->subject('Order Invoice');
                 });
            
            return redirect()->back()->with('message','We have send your reset password link on your mail id.');
        }
        else{
            return redirect()->back()->with('message','Invaid Email Id.');
        }
        
        
  
        
    }
}
