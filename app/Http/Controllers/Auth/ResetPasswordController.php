<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\User;
use Hash,Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function getResetPassword($id)
    {
        $user = User::where('id',$id)->first();
        
        return view('auth.passwords.reset',compact('user'));
    }
     public function postResetPassword(Request $request)
    {
        
         $data = $request->all();
      
    if($data['password'] == $data['password_confirmation']){
        $user = $data['email'];
        User::where('email',$user)->update(['password'=> Hash::make($data['password'])]);
        $user = User::where('email',$user)->first();
        Auth::login($user);
        return redirect()->route('home');
    }
    else{
        return redirect()->back()->with('message','Password did not match');

    }
    }
    
    
    
}
