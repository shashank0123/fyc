<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Testimonial;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Category;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Review;
use App\Wishlist;
use App\Models\Banner;
use App\Models\Company;
use App\Models\Query;
use App\Models\Faq;
use App\Models\Newsletter;
use App\Models\Help;
use Log;
use DB;
use Auth;
use Mail;

class FycController extends Controller
{
    public function index(){
    	$banners = Banner::where('status','Active')
        ->where('image_type','slider')
        ->orderBy('created_at','DESC')
        ->get();
        $full_banners = Banner::where('status','Active')
        ->where('image_type','full')
        ->orderBy('created_at','DESC')
        ->get();
        $medium_banner = Banner::where('image_type','medium')
        ->orderBy('created_at','DESC')
        ->first();
        $products = Product::where('status','Active')
        ->orderBy('created_at','DESC')
        ->orderBy('sell_price','ASC')
        ->get();
        $best_products = Product::where('trending','yes')
        ->orderBy('created_at','DESC')
        ->get();
        $testimonials = Testimonial::where('status','Active')
        ->orderBy('created_at','DESC')
        ->get();
        return view('fyc.index',compact('products','best_products','banners','medium_banner','testimonials','full_banners'));
    }

    // public function getHome(){
    //  $companyInfo = Company::first();
    //  $maincategory = Category::where('status','Active')->where('category_id',0)->get();
    //  $slider_cat = Category::where('status','Active')->where('category_id',0)->orderBy('created_at','DESC')->limit(5)->get();
    //  $slider_banners = Banner::where('image_type','slider')->where('status','Active')->get();
        
    //  $medium_banners = Banner::where('image_type','medium')->where('status','Active')->get();

    //  $featured_products = Product::where('trending','yes')->orderBy('created_at','DESC')->limit(25)->get();
    //  $new_products = Product::orderBy('name','ASC')->orderBy('created_at','DESC')->limit(25)->get();
    //  $trending_products = Product::orderBy('created_at','DESC')->limit(3)->get();
        
    //  $offers = Offer::where('status','Active')->orderBy('created_at','DESC')->limit(3)->get();
        
    //  return view('fyc-web.home', compact('companyInfo','maincategory','slider_banners', 'medium_banners','featured_products','new_products','offers','slider_cat','trending_products'));
    //  // return view('fyc-web.home',compact('companyInfo'));
    // }


    public function getProducts(Request $request, $slug){
        if($slug == 'all'){
            $searched_products = Product::where('status','Active')
            ->get();
        }
        elseif($slug == 'trending'){
            $searched_products = Product::where('status','Active')
            ->where('trending','yes')
            ->get();
        }
        else{
            $searched_products = Product::leftJoin('categories','categories.id','products.category_id')
            ->where('categories.slug',$slug)
            ->select('products.*')
            ->get();
        }

        $categories = Category::where('status','Active')
        ->orderBy('created_at','DESC')
        ->get();

        $max = Product::orderBy('sell_price','DESC')->first();

        return view('fyc.products',compact('searched_products','categories','slug','max'));
    }

    public function getOffers(){
        $offers = Offer::where('status','Active')
        ->get();
        return view('fyc-web.offers',compact('offers'));
    }




    public function getAbout(){
        return view('fyc-web.about-us');
    }

    public function getCart(){
        return view('fyc-web.cart');
    }

    public function getCheckout(){
        $productDetails = array();
        $totalAmount = 0;
        if (session()->get('cart')) {
            foreach (session()->get('cart') as $key => $value) {
              $product = Product::where('id', $value->product_id)->first();
              $product['quantity'] = $value->quantity;
              $product['total_price'] = $product->sell_price * $value->quantity;
              $totalAmount += $product['total_price'];
              array_push($productDetails, $product);
          }
      } else {
        return redirect('/');
    }

    $success ='';
    $address = Address::where('user_id', Auth::user()->id)->first();
    if(isset($address))
      $allAddress = Address::where('user_id', Auth::user()->id)->where('id','!=',$address->id)->get();
  else
    $allAddress="";

      
return view('fyc-web.checkout', compact('productDetails', 'totalAmount', 'success', 'address', 'allAddress'));

}

public function getContact(){
   $companyInfo = Company::first();
   return view('fyc-web.contact',compact('companyInfo'));
}

public function getFaq(){
    return view('fyc-web.faq');
}

public function getLogin(){
    return view('fyc-web.login');
}

public function getAccount(){
    $user = Auth::user();
    $addresses = "";
    $orderDetails = DB::table('orders')->where('user_id', $user->id)->where('status','!=','in_process')->get();
    $address = Address::where('user_id', $user->id)->first();
    $upd_add = Address::where('user_id', $user->id)->first();

    if(!empty($address))
      $addresses = Address::where('user_id', $user->id)->where('id','!=',$address->id)->get();
  $help = Help::where('user_id', $user->id)->get();
  
  return view('fyc-web.my-account', compact('user', 'orderDetails', 'address', 'help','addresses','upd_add'));
}

public function getProductDetails(Request $request,$slug){
    $product = Product::where('slug',$slug)->first();
    $category = Category::where('id',$product->category_id)->first();
    $related_products = Product::where('category_id',$product->category_id)->get();
    $productReview = Review::where('product_id', $product->id)->avg('rating');
    $countReview = Review::where('product_id', $product->id)->count();

    $productReview = floor($productReview);
    $reviews = Review::where('product_id',$product->id)->get();
    return view('fyc-web.product-details',compact('product','category','related_products','productReview','countReview','reviews'));
}



public function getRegister(){
    return view('fyc-web.register');
}


public function getWishlist(Request $request, $id){
    $wishlist = Wishlist::where('user_id',$id)->get();

    return view('fyc-web.wishlist',compact('wishlist'));

}

public function getSeller(){
    $offers = Offer::where('status','Active')->orderBy('created_at','DESC')->get();
    return view('fyc-web.seller',compact('offers'));
}

public function getOfferDetail(Request $request){
    $id = $request->id;
    $offer = Offer::find($id);
    return response()->json(['offer'=>$offer]);
}
}
