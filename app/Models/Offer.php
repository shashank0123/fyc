<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Offer extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'offers';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['image_url','offer_title','offer_sub_title','offer_short_description','offer_long_description','status'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function setImageUrlAttribute($value)
    {
        $attribute_name = "image_url";
        $disk = "public";
        $destination_path = "offers";       
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path)
      {
          $request = \Request::instance();

          // if a new file is uploaded, delete the file from the disk
          if ($request->hasFile($attribute_name) &&
              $this->{$attribute_name} &&
              $this->{$attribute_name} != null) {
              \Storage::disk($disk)->delete($this->{$attribute_name});
              $this->attributes[$attribute_name] = null;
          }

          // if the file input is empty, delete the file from the disk
          if (is_null($value) && $this->{$attribute_name} != null) {
              \Storage::disk($disk)->delete($this->{$attribute_name});
              $this->attributes[$attribute_name] = null;
          }

          // if a new file is uploaded, store it on disk and its filename in the database
          if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
              // 1. Generate a new file name
              $file = $request->file($attribute_name);
              $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

              // 2. Move the new file to the correct path
              $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

              // 3. Save the complete path to the database
              $this->attributes[$attribute_name] = 'storage/' . $file_path;
          }
      }
}
