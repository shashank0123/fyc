<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Log;

class Order extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'orders';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['user_id', 'address', 'price', 'payment_method', 'transaction_id', 'encrypted_payment_id', 'shipping_address' ,'status'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    

    public function getItemCountAttribute()
    {
        // return \DB::table('order_items')->where('order_id', $this->id)
        // ->count('item_id');
    }

    // public function getIdAttribute($value)
    // {
    //     $message = " ";
    //     $products = \DB::table('order_items')
    //     ->leftjoin('products', 'products.id', '=', 'order_items.item_id')
    //     ->where('order_id', $value)
    //     ->select('products.name', 'order_items.quantity', 'order_items.price')
    //     ->get();
        
    //     if($products){
    //         foreach($products as $product){
    //             $message = $message.$product->quantity." X ".ucfirst($product->name)."<br>";
    //         }
    //     }
    //     return $message;
    // }

    public function getPaymentMethodAttribute($value)
    {
      return $this->attributes['payment_method'] =  str_replace('_', ' ', $value);
  }

  public function getStatusAttribute($value)
  {
      return $this->attributes['status'] =  str_replace('_', ' ', $value);
  }

  public function getShippingAddressAttribute($value)
  {
        $add = Address::find($value);
        $value = $add['address']??''.", ".$add['city']??''." , ".$add['state']??''." (".$add->pin_code??''.")";       
        return  $value;
  }

   public function getUserIdAttribute($value)
  {
        $add = \App\User::find($value);
        $value = $add['name'];
        
        return ucfirst($value);
  }
    // public function getOrderItemAttribute()
    // {
    //     return \DB::table('order_items')->where('order_id', $this->id)
    //                                    ->get();
    // }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


    

}

