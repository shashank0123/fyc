<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('product', 'ProductCrudController');
    CRUD::resource('offer', 'OfferCrudController');
    CRUD::resource('banner', 'BannerCrudController');
    CRUD::resource('blog', 'BlogCrudController');
    CRUD::resource('newsletter', 'NewsletterCrudController');
    CRUD::resource('testimonial', 'TestimonialCrudController');
    CRUD::resource('order', 'OrderCrudController');
    CRUD::resource('customer', 'CustomerCrudController');
    CRUD::resource('company-info', 'CompanyCrudController');
    CRUD::resource('faq', 'FaqCrudController');
    CRUD::resource('review', 'ReviewCrudController');
    CRUD::resource('help', 'HelpCrudController');
    CRUD::resource('reply', 'ReplyCrudController');
    CRUD::resource('report', 'ReportCrudController');
    CRUD::resource('subcategory', 'SubcategoryCrudController');
    CRUD::resource('delivered', 'ReportCrudController');
    CRUD::resource('returened', 'ReportCrudController');
    CRUD::resource('shipped', 'ReportCrudController');
    CRUD::resource('pending', 'ReportCrudController');
    CRUD::resource('headline', 'HeadlineCrudController');
}); // this should be the absolute last line of this file