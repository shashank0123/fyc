<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FycController@index');
Route::get('/offers', 'FycController@getOffers');
Route::get('/products/{slug}','FycController@getProducts');
// Route::get('/{slug}','FycController@getProducts');


Route::post('/sendEmail', 'Auth\ForgotPasswordController@sendEmail')->name('password.emails');
Route::get('/reset-password/{id}', 'Auth\ResetPasswordController@getResetPassword')->name('password.reset');
Route::post('/reset-password', 'Auth\ResetPasswordController@postResetPassword')->name('password.updates');

Route::get('/', 'FycController@index')->name('home');
Route::get('/about', 'FycController@getAbout');
Route::get('/sell-on-homeglare', 'FycController@getSeller');
Route::post('/offer-detail', 'FycController@getOfferDetail');
Route::get('/cart/{id}', 'FycController@getCart');
Route::get('/checkout/{id}', 'FycController@getCheckout');
Route::get('/contact', 'FycController@getContact');
Route::post('/contact',  'SiteController@contactUs');
Route::get('/faq', 'FycController@getFaq');
Route::get('/account/{id}', 'FycController@getAccount');
Route::get('/product-detail/{slug}', 'FycController@getProductDetails');
// Route::get('/product/{slug}', 'FycController@getProducts');
// Route::get('/register', 'FycController@getRegister');
Route::get('/wishlist/{id}', 'FycController@getWishlist');

Route::post('pay', 'PayController@pay');
Route::get('payment-success', 'PayController@success');

Route::get('/auth/redirect/facebook', 'SocialController@redirect');
Route::get('/callback/facebook', 'SocialController@callback');

Route::get('/redirect/google', 'SocialAuthGoogleController@redirect');
Route::get('/callback/google', 'SocialAuthGoogleController@callback');

Route::post('/add-to-cart','AjaxController@addToCart');
Route::post('/add-to-wishlist','AjaxController@addToWishlist');
Route::post('/delete-wishlist-item','AjaxController@deleteWishlistItem');
Route::post('/newsletter', 'SiteController@createNewsletter')->name('newsletter');
Route::post('/cart/delete-product/{id}','AjaxController@deleteSessionData');
Route::post('/update-cart','AjaxController@updateCart');

Auth::routes();

// Route::get('/home', 'FycController@getHome');
// Route::get('/', 'FycController@getHome');
Route::post('/login', 'Auth\LoginController@postLogin');
Route::post('/update-password', 'AjaxController@updatePassword');
Route::post('/update-profile', 'AjaxController@updateProfile');

Route::post('/order-product', 'SiteController@orderProduct')->name('order-product');
Route::get('/order-booked', function()
{
  return view('fyc-web.ordersuccess');
});

// Route::get('/about-us','SiteController@getAboutUs');
// Route::get('/contact','SiteController@getContactUs');
// Route::post('/contact-us', 'SiteController@contactUs')->name('contact-us');
Route::get('/faq','SiteController@getFaq')->name('faq');
Route::get('/terms&conditions','SiteController@getTermsandCondition');
Route::get('/privacy-policy','SiteController@getPrivacyPolicy');
Route::get('/return-policy','SiteController@getReturnPolicy');
Route::get('/my-account/{id}','SiteController@getMyAccount');
Route::get('/ordered-item-details/{order_id}', 'SiteController@orderedItemDetails')->name('ordered-item-details');
Route::get('/ordered-item-track/{order_id}', 'SiteController@orderedItemTrack')->name('ordered-item-track');
Route::resource('reply', 'ReplyController');

Route::get('/show-minicart','AjaxController@showMiniCart');
Route::get('/search-product/productsCat','AjaxController@showProductCat');
Route::get('/search-product/sort', 'AjaxController@sorting');
Route::get('/search-product/price-filter', 'AjaxController@price');
Route::get('/search-product/colored', 'AjaxController@colored');
Route::get('/search-product/rating', 'AjaxController@rating');
Route::resource('help-support', 'HelpSupportController');

Route::get('/get-product_details-data/{productId}', 'AjaxController@getProductDetailsData');
Route::post('/give-product-rating', 'SiteController@giveProductRating')->name('give-product-rating');
Route::post('/global-search-product', 'SiteController@globalSearchProduct')->name('global-search-product');

// snapSocialShare
Route::get('/shareLinkOnSocialSite/{siteKey}/{slug_url}/{product_id}', 'SiteController@shareLinkOnSocialSite');
//fb snap Social Share callback
Route::get('SharedLink/callback/{siteKey}', 'SiteController@SharedLinkCallback');
